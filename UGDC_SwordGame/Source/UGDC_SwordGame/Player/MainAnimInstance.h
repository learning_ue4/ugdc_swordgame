// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "UGDC_SwordGame/Common/CommonAnimInstance.h"
#include "MainAnimInstance.generated.h"

/*
 * Animation instance of main character
 */
UCLASS()
class UGDC_SWORDGAME_API UMainAnimInstance final : public UCommonAnimInstance
{
	GENERATED_BODY()

private:
	/* Main Character - the player itself */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class AMainCharacter* MainCharacter;

	/* If character that holds this animation is alive */
	UFUNCTION(BlueprintPure, meta = (BlueprintThreadSafe = "true"))
	bool IsCharacterAlive() const;

	void NativeInitializeAnimation() override;
	
	/* An analogue of Tick() function */
	void UpdateAnimationProperties() override;
};
