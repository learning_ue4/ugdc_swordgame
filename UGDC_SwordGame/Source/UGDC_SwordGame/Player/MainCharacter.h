// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "UGDC_SwordGame/Common/GameCharacter.h"
#include "MainPlayerController.h"
#include "UGDC_SwordGame/Items/Weapon.h"
#include "MainCharacter.generated.h"

/*
 * Enum, holding health statuses of a player
 */
UENUM(BlueprintType)
enum class EHealthStatus : uint8
{
	EHS_Normal			UMETA(DisplayName = "Normal"),
	EHS_BelowMinimum	UMETA(DisplayName = "BelowMinimum"),
	EHS_MAX				UMETA(DisplayNamw = "DefaultMAX")
};

/*
 * Enum, holding movement statuses, that a player can have
 */
UENUM(BlueprintType)
enum class EMovementStatus : uint8
{
	EMS_Still		UMETA(DisplayName = "Still"),
	EMS_Normal		UMETA(DisplayName = "Normal"),
	EMS_Sprinting	UMETA(DisplayName = "Sprinting"),
	EMS_Walking		UMETA(DisplayName = "Walking"),
	EMS_Dead		UMETA(DisplayName = "Dead"),
	EMS_MAX			UMETA(DisplayNamw = "DefaultMAX")
};

/*
 * Enum, holding stamina statuses, that player's stamina can be in
 */
UENUM(BlueprintType)
enum class EStaminaStatus : uint8
{
	ESS_Normal				UMETA(DisplayName = "Normal"),
	ESS_BelowMinimum		UMETA(DisplayName = "BelowMinimum"),
	ESS_Exhausted			UMETA(DisplayName = "Exhausted"),
	ESS_ExhaustedRecovering	UMETA(DisplayName = "ExhaustedRecovering"),
	ESS_MAX					UMETA(DisplayNamw = "DefaultMAX")
};

/*
 * Character, controlled by the player.
 * Responsible for player's initial parameters, movement, camera control and
 * all thea actions performed by the player. 
 */
UCLASS()
class UGDC_SWORDGAME_API AMainCharacter final : public AGameCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMainCharacter();

	/* Switch the level that character is currently in */
	void SwitchLevel(FName LevelName) const;
	
	/* *****FOR DEBUG***** */
	/* Array of locations of items picked up on the level (for debug purposes only) */
	TArray<FVector> PickupLocations;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* Blueprint event firing when item is overlapped */
	UFUNCTION(BlueprintImplementableEvent, Category = Consumable)
	void OnCameraRotationReset();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	/*
	 * Sets the weapon to be equipped by the character
	 * and removes the currently equipped weapon
	 */
	void SetEquippedWeapon(class AWeapon* WeaponToEquip);
	
	FORCEINLINE void SetActiveOverlappingItem(class AItem* Item)
	{
		ActiveOverlappingItem = Item;
		bHasActiveOverlappingItem = (Item != nullptr);
	}

	UFUNCTION(BlueprintCallable, Category = Items)
	FORCEINLINE bool HasOverlappingItem() const { return bHasActiveOverlappingItem; }

	/* Check interacting actors and update combat target respectively */
	void UpdateClosestCombatTarget();
	
	FORCEINLINE AMainPlayerController* GetPlayerController() const { return PlayerController; }

	/* Is character alive */
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	bool IsAlive() const;

	/* *****FOR DEBUG***** */
	/* Draw a debug sphere above all locations of picked up items */
	UFUNCTION(BlueprintCallable)
	void ShowPickupLocations();

private:
	/* ====================================== */
	/* ======== CAMERA ====================== */
	/* A boom arm, positioning the camera behind the player	 */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/* A camera that's hanging on the boom arm */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

	/* Technical player stats for movement calculations */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float CameraBoomLength;

	/* Base turn rates to scale turning functions or the camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float BaseTurnRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float BaseLookUpRate;	
	
	/* ====================================== */
	/* ======== CAPSULE ===================== */
	/* Character capsule's half height */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = CollisionCapsule, meta = (AllowPrivateAccess = "true"))
	float CharacterCapsuleHalfHeight;

	/* Character capsule's half width */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = CollisionCapsule, meta = (AllowPrivateAccess = "true"))
	float CharacterCapsuleWidth;
	
	/* ====================================== */
	/* ======== COINS ======================= */
	int32 CoinCount;
	
	/* ====================================== */
	/* ======== COMBAT ====================== */
	/* Used to filter surrounding objects in search for possible combat targets */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AEnemy> CombatTargetClassFilter;

	/* Current player's combat target */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	FVector CombatTargetLocation;
	
	/* ====================================== */
	/* ======== CONTROLLER ================== */
	/* A reference to the controller to get HUD info */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Controller, meta = (AllowPrivateAccess = "true"))
	AMainPlayerController* PlayerController;

	bool bIsGamePadUsed;
	
	/* ====================================== */
	/* ======== INPUT ======================= */
	/* Indicates if spring key is down */
	bool bIsSprintKeyDown;

	/* Indicates if walk slow key is down */
	bool bIsWalkSlowKeyDown;

	/* Indicates if LMB is down */
	bool bIsLMBDown;

	/* Indicates if Pause button is down */
	bool bIsPauseButtonDown;

	/* Indicates if Use button is down */
	bool bIsUseButtonDown;
	
	/* ====================================== */
	/* ======== ITEMS ======================= */
	/* Item that is currently overlapped by player */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Items, meta = (AllowPrivateAccess = "true"))
	AItem* ActiveOverlappingItem;

	/* Does player have an overlapping item */
	bool bHasActiveOverlappingItem;

	/* Items that are currently stored in inventory */
	UPROPERTY(EditDefaultsOnly, Category = SavedData, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class AItemStorage> ItemStorage;
	
	/* ====================================== */
	/* ======== MOVEMENT ==================== */	
	/* Current movement status of the player */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Enums, meta = (AllowPrivateAccess = "true"))
	EMovementStatus MovementStatus;

	/* Rate at which player turns to pointed direction */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float RotationRateYaw;

	/* Jump height */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float JumpZVelocity;

	/* Movement velocity in air that affects player's position */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float AirControlMeasure;

	/* Is character currently moving forward */
	bool bIsMovingForward;

	/* Is character currently moving right */
	bool bIsMovingRight;

	/* Speed at which running begins */
	float RunningSpeed;

	/* Speed at which sprinting begins */
	float SprintingSpeed;

	/* Speed at which walking with pressed button begins */
	float WalkingSpeed;
	
	/* ====================================== */
	/* ======== STATS: HEALTH =============== */	
	/* Current health status of the player */
	UPROPERTY(VisibleAnywhere, Category = Enums, meta = (AllowPrivateAccess = "true"))
	EHealthStatus HealthStatus;

	/* Units of health recovered per time frame */
	float HealthRecoveryRate;
	
	/* ====================================== */
	/* ======== STATS: STAMINA ============== */
	/* Current stamina status of the player */
	UPROPERTY(VisibleAnywhere, Category = Enums, meta = (AllowPrivateAccess = "true"))
	EStaminaStatus StaminaStatus;
	
	/* Maximum available stamina */
	float MaxStamina;

	/* Current stamina value */
	float Stamina;

	/* Units of stamina drained per second of sprinting */
	float StaminaSprintDrainRate;

	/* Units of stamina recovered per second */
	float StaminaRecoveryRate;

	/* Stamina recovery rate multiplier when walking slow */
	float StaminaRecoveryWalkingMultiplier;

	/* Stamina recovery rate multiplier when standing still */
	float StaminaRecoveryStillMultiplier;

	/* Minimum stamina required for sprinting */
	float MinSprintStamina;

	/* Minimum stamina required for starting health recovery */
	float MinStaminaForRecovery;

	/* ====================================== */
	/* ======== WEAPON ====================== */
	/* Weapon, equipped in right hand */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Items, meta = (AllowPrivateAccess = "true"))
	AWeapon* EquippedWeapon;
	
private:
	/* ====================================== */
	/* ======== CAMERA ====================== */
	/* Force camera to return to it's initial angle */
	void ForceCenterCamera();
	
	/* ====================================== */
	/* ======== COINS ======================= */
	UFUNCTION(BlueprintCallable, Category = Coins, meta = (AllowPrivateAccess = "true"))
	FORCEINLINE int GetCoinsCount() const { return CoinCount; }

	/* Gives a certain amount of coins to player */
	UFUNCTION(BlueprintCallable, Category = Coins, meta = (AllowPrivateAccess = "true"))
	void IncrementCoins(const int32& Amount);

	/* ====================================== */
	/* ======== CONSTRUCTION ================ */	
	/* Initialize all parameters with default values */
	void InitializeDefaultValues();

	/* Initialize camera and its boom arm */
	void InitializeCamera();

	/* Configure character's movement */
	void ConfigureMovement() const;	

	/* Update health status and recover health if needed */
	void UpdateHealthStatus(const float& DeltaTime);

	void InitializeUserSettings();
	
	/* ====================================== */
	/* ======== COMBAT ====================== */
	/* Go into attacking state and perform an attack */
	void AttackStart() override;

	/* End attack and go out of attacking state */
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	void AttackEnd();

	/* Sets combat target location in tick function */
	void SetCombatTargetLocation();

	/* ====================================== */
	/* ======== DEATH ======================= */
	/* Pass away */
	void Die(AActor* DeathCauser) override;
	
	/* What happens after death animation end */
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	void DeathEnd();

	/* ====================================== */
	/* ======== INPUT ======================= */
	/* Pressed down to start sprinting */
	void SprintKeyUp();

	/* Released to stop sprinting */
	void SprintKeyDown();

	/* Pressed down to start walking slow */
	void WalkSlowKeyUp();

	/* Released to stop walking slow */
	void WalkSlowKeyDown();

	/* Pressed down to start LMB-action (depends on the context) */
	void LMBDown();

	/* Pressed down to stop LMB-action (depends on the context) */
	void LMBUp();

	/* Pressed down to use object (interact with it) */
	void UseKeyDown();

	/* Pressed down to Pause the game */
	void PauseButtonDown();

	/* Pressed down to Center the camera */
	void CenterCameraButtonDown();

	/* ====================================== */
	/* ======== MOVEMENT ==================== */
	/* Setting the movement status and running speed */
	void SetMovementStatus(EMovementStatus Status);

	/* Check if player is walking slow and speed down accordingly */
	void CheckWalkingStatus();
	
	/* Called for forwards/backwards input */
	void MoveForward(float MovementValue);

	/* Called for side to side input */
	void MoveRight(float MovementValue);

	/* Called via input to turn at a given rate
	 * @param Rate - normalized rate, i.e. 1.0 means 100% of desired rate
	 */
	void TurnAtRate(float Rate);

	/* Called via input to loot up/down at a given rate
	 * @param Rate - normalized rate, i.e. 1.0 means 100% of desired rate
	*/
	void LookUpAtRate(float Rate);
	
	/* Move character in selected axis */
	void MoveInAxis(const float& MovementValue, const EAxis::Type& Axis);

	/* Jump */
	virtual void Jump() override;

	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	FORCEINLINE EMovementStatus GetMovementStatus() const { return MovementStatus; }
	
	/* ====================================== */
	/* ======== SAVE/LOAD =================== */
	/* Save all player's data necessary for level transitioning */
	UFUNCTION(BlueprintCallable)
	void SaveGameData() const;

	/* Load all previously saved data */
	UFUNCTION(BlueprintCallable)
	void LoadGameData(bool IsSetPositionNeeded, bool IsLevelSwitchNeeded);

	/* ====================================== */
	/* ======== SFX ========================= */
	/* Sound played when attacking with weapon (the particular sound defined in the weapon) */
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	void PlayWeaponAttackSound() const;

	/* ====================================== */
	/* ======== STATS: HEALTH =============== */
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	FORCEINLINE EHealthStatus GetHealthStatus() const { return HealthStatus; }

	/* ====================================== */
	/* ======== STATS: STAMINA ============== */
	/* Check stamina status and limit character sprinting and specials or recover stamina if needed */
	void UpdateStaminaStatus(const float& DeltaTime);	

	UFUNCTION(BlueprintCallable, Category = PlayerStats, meta = (AllowPrivateAccess = "true"))
	FORCEINLINE float GetStamina() const { return Stamina; }

	UFUNCTION(BlueprintCallable, Category = PlayerStats, meta = (AllowPrivateAccess = "true"))
	FORCEINLINE float GetMaxStamina() const { return MaxStamina; }

	UFUNCTION(BlueprintCallable, Category = Stats)
	void IncrementStamina(const float& Amount);

	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	FORCEINLINE EStaminaStatus GetStaminaStatus() const { return StaminaStatus; }
};
