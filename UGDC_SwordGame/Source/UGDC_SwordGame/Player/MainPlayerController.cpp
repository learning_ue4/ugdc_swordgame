// Fill out your copyright notice in the Description page of Project Settings.


#include "MainPlayerController.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"

void AMainPlayerController::BeginPlay()
{
	Super::BeginPlay();

	InitHUD();
	InitEnemyHealthBar();
	InitPauseMenu();
	InitUseItemWidget();
}

void AMainPlayerController::InitHUD()
{
	if (IsValid(HUDOverlayAsset))
	{
		HUDOverlay = CreateWidget<UUserWidget>(this, HUDOverlayAsset);
		HUDOverlay->AddToViewport();
		HUDOverlay->SetVisibility(ESlateVisibility::Visible);
	}
}

void AMainPlayerController::InitEnemyHealthBar()
{
	if (IsValid(WEnemyHealthBar))
	{
		EnemyHealthBar = CreateWidget<UUserWidget>(this, WEnemyHealthBar);
		if (IsValid(EnemyHealthBar))
		{
			EnemyHealthBar->AddToViewport();
			EnemyHealthBar->SetVisibility(ESlateVisibility::Hidden);
		}
	}
}

void AMainPlayerController::InitPauseMenu()
{
	if (IsValid(WPauseMenu))
	{
		PauseMenu = CreateWidget<UUserWidget>(this, WPauseMenu);
		if (IsValid(PauseMenu))
		{
			PauseMenu->AddToViewport();
			PauseMenu->SetVisibility(ESlateVisibility::Hidden);
		}
	}
}

void AMainPlayerController::InitUseItemWidget()
{
	if (IsValid(WUseItemWidget))
	{
		UseItemWidget = CreateWidget<UUserWidget>(this, WUseItemWidget);
		if (IsValid(UseItemWidget))
		{
			UseItemWidget->AddToViewport();
			UseItemWidget->SetVisibility(ESlateVisibility::Hidden);
		}		
	}
}

void AMainPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetEnemyHealthBarViewPortPosition();
	SetUseItemWidgetViewPortPosition();
};

void AMainPlayerController::SetEnemyHealthBarViewPortPosition()
{
	if (!IsValid(EnemyHealthBar)) return;

	FVector2D ViewPortPosition;
	ProjectWorldLocationToScreen(EnemyLocation, ViewPortPosition);
	ViewPortPosition.Y -= 100.F;

	const auto ViewPortSize = FVector2D(200.0F, 25.0F);
	EnemyHealthBar->SetPositionInViewport(ViewPortPosition);
	EnemyHealthBar->SetDesiredSizeInViewport(ViewPortSize);

	const auto Alignment = FVector2D(.0F, .0F);
	EnemyHealthBar->SetAlignmentInViewport(Alignment);
}

void AMainPlayerController::DisplayEnemyHealthBar()
{
	if (!IsValid(EnemyHealthBar)) return;
	
	bIsEnemyHealthBarVisible = true;
	EnemyHealthBar->SetVisibility(ESlateVisibility::Visible);
}

void AMainPlayerController::HideEnemyHealthBar()
{
	if (!IsValid(EnemyHealthBar)) return;

	bIsEnemyHealthBarVisible = false;
	EnemyHealthBar->SetVisibility(ESlateVisibility::Hidden);
}

void AMainPlayerController::DisplayPauseMenu_Implementation()
{
	if (!IsValid(PauseMenu)) return;

	bIsPauseMenuVisible = true;
	PauseMenu->SetVisibility(ESlateVisibility::Visible);

	SetPauseMenuInputMode(true);
}

void AMainPlayerController::HidePauseMenu_Implementation()
{
	if (!IsValid(PauseMenu)) return;

	bIsPauseMenuVisible = false;
	SetPauseMenuInputMode(false);
}

void AMainPlayerController::SetUseItemWidgetViewPortPosition()
{
	if (!IsValid(UseItemWidget)) return;

	FVector2D ViewPortPosition;
	ProjectWorldLocationToScreen(UsableItemLocation, ViewPortPosition);
	ViewPortPosition.Y += 20.F;
	const auto ViewPortSize = FVector2D(25.0F, 25.0F);
	UseItemWidget->SetPositionInViewport(ViewPortPosition);
	UseItemWidget->SetDesiredSizeInViewport(ViewPortSize);

	const auto Alignment = FVector2D(.0F, .0F);
	UseItemWidget->SetAlignmentInViewport(Alignment);	
}

void AMainPlayerController::DisplayUseItemWidget()
{
	if (!IsValid(UseItemWidget)) return;

	bIsUserItemWidgetVisible = true;
	UseItemWidget->SetVisibility(ESlateVisibility::Visible);
}

void AMainPlayerController::HideUseItemWidget()
{
	if (!IsValid(UseItemWidget)) return;

	bIsUserItemWidgetVisible = false;
	UseItemWidget->SetVisibility(ESlateVisibility::Hidden);
}

void AMainPlayerController::TogglePauseMenu()
{
	if (!IsValid(PauseMenu)) return;

	if (bIsPauseMenuVisible)
	{
		// The implementation is called from blueprint as parent function,
		// but from here we shall call a "full version" - BP and C++ (which is called from BP)
		HidePauseMenu();
	}
	else
	{
		DisplayPauseMenu();
	}
}

void AMainPlayerController::SetPauseMenuInputMode(const bool& bIsActive)
{
	if (bIsActive)
	{
		SetInputMode(FInputModeGameAndUI());
	}
	else
	{		
		SetInputGameModeOnly();
	}

	bShowMouseCursor = bIsActive; // in Controller
	UGameplayStatics::SetGamePaused(GetWorld(), bIsActive);
}

void AMainPlayerController::SetInputGameModeOnly()
{
	SetInputMode(FInputModeGameOnly());
}