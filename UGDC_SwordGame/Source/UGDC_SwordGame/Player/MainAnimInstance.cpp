// Fill out your copyright notice in the Description page of Project Settings.


#include "MainAnimInstance.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "MainCharacter.h"

void UMainAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	const auto MainCharacterPawn = GetCharacterPawn();
	
	if (IsValid(MainCharacterPawn))
	{
		MainCharacter = Cast<AMainCharacter>(MainCharacterPawn);
	}
	else
	{
		MainCharacter = nullptr;
	}
}

void UMainAnimInstance::UpdateAnimationProperties()
{
	Super::UpdateAnimationProperties();	

	const auto MainCharacterPawn = GetCharacterPawn();
	
	if (MainCharacterPawn)
	{		
		const auto CharacterSpeed = MainCharacterPawn->GetVelocity();

		// When on ground - don't care about Z speed (Z speed is for jumpin' and fallin' and has separate logic
		const auto LateralSpeed = FVector(CharacterSpeed.X, CharacterSpeed.Y, 0.F);
		SetMovementSpeed(LateralSpeed.Size());

		const auto bIsFalling = MainCharacterPawn->GetMovementComponent()->IsFalling();
		SetInAir(bIsFalling);

		if (MainCharacter == nullptr)
		{
			MainCharacter = Cast<AMainCharacter>(MainCharacterPawn);
		}
	}
}

bool UMainAnimInstance::IsCharacterAlive() const
{
	// if character is not valid, presume that he's alive
	if (!IsValid(MainCharacter)) return true;

	return MainCharacter->IsAlive();
}