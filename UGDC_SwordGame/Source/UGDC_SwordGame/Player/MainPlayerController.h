 // Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MainPlayerController.generated.h"

/**
 * A controller for main character, holding
 * HUD and other related logic
 */
UCLASS()
class UGDC_SWORDGAME_API AMainPlayerController final : public APlayerController
{
	GENERATED_BODY()

public:	
	/* Show enemy health bar in the HUD */
	void DisplayEnemyHealthBar();

	/* Hide enemy health bar from the HUD */
	void HideEnemyHealthBar();

	FORCEINLINE void SetEnemyLocation(const FVector& LocationToSet) { EnemyLocation = LocationToSet; }

	/* Switch pause menu visibility */
	void TogglePauseMenu();

	/* Set input mode to Game Mode Only */
	void SetInputGameModeOnly();

	/* Show pause menu in the HUD */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = HUD, meta = (AllowPrivateAccess = "true"))
	void DisplayPauseMenu();

	/* Hide pause menu from the HUD */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = HUD, meta = (AllowPrivateAccess = "true"))
	void HidePauseMenu();

	FORCEINLINE bool IsPauseMenuVisible() const { return bIsPauseMenuVisible; }

	/* Show use-item widget in the HUD */
	void DisplayUseItemWidget();

	/* Hide use-item widget from the HUD */
	void HideUseItemWidget();
	
	FORCEINLINE void SetUsableItemLocation(const FVector& LocationToSet) { UsableItemLocation = LocationToSet; }

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;	

private:
	/* Reference to the UMG asset in the editor */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<class UUserWidget> HUDOverlayAsset;

	/* Variable to hold the widget after creating it */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	UUserWidget* HUDOverlay;

	/* Reference to enemy health bar widget in editor */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UUserWidget> WEnemyHealthBar;

	/* Enemy health bar itself */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	UUserWidget* EnemyHealthBar;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	bool bIsEnemyHealthBarVisible;

	/* Reference to the pause menu widget in editor */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UUserWidget> WPauseMenu;

	/* Pause menu itself */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	UUserWidget* PauseMenu;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	bool bIsPauseMenuVisible;

	/* Reference to the UMG asset in the editor */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UUserWidget> WUseItemWidget;

	/* Widget that shows that the overlapped item can be used (equipped, placed in inventory, etc.)  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	UUserWidget* UseItemWidget;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = HUDWidgets, meta = (AllowPrivateAccess = "true"))
	bool bIsUserItemWidgetVisible;

	/* Location of the current player's combat target */
	FVector EnemyLocation;

	/* Location of the current player's usable item */
	FVector UsableItemLocation;
	
	/* Init main HUD w/all player's stats */
	void InitHUD();

	/* Init health bar widget */
	void InitEnemyHealthBar();

	/* Init in-game pause menu */
	void InitPauseMenu();

	/* Init use-item widget */
	void InitUseItemWidget();
	
	/* Set the viewport position of an enemy's health bar */
	void SetEnemyHealthBarViewPortPosition();

	/* Set the viewport position of a "Use Item" widget */
	void SetUseItemWidgetViewPortPosition();

	/* Set the input mode of the pause menu */
	void SetPauseMenuInputMode(const bool& bIsActive);
};
