// Fill out your copyright notice in the Description page of Project Settings.

#include "MainCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "Engine/World.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "UGDC_SwordGame/Items/Weapon.h"
#include "UGDC_SwordGame/NPC/Enemy.h"
#include "UGDC_SwordGame/GameplayActors/UGDCSaveGame.h"
#include "UGDC_SwordGame/GameplayActors/ItemStorage.h"
#include "UGDC_SwordGame/Common/InteractInterface.h"
#include "Animation/AnimInstance.h"
#include "MainPlayerController.h"
#include "Sound/SoundCue.h"
#include "Logger.h"
#include "UGDC_SwordGame/Tech/UGDCGameUserSettings.h"
#include "Engine/Engine.h"


// Sets default values
AMainCharacter::AMainCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;	

	InitializeDefaultValues();
	InitializeCamera();
	ConfigureMovement();		
}

void AMainCharacter::InitializeCamera()
{	
	RootComponent = GetRootComponent();
	// Create camera boom (pulls towards the player in case of collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = CameraBoomLength;
	CameraBoom->bUsePawnControlRotation = true;

	GetCapsuleComponent()->InitCapsuleSize(CharacterCapsuleWidth, CharacterCapsuleHalfHeight);

	// Create and setup follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	// Camera is at the end of the boom and boom is adjusted to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false;

	// Don't rotate when the controller rotates,
	// let that just affect the camera
	bUseControllerRotationYaw = false;
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
}

void AMainCharacter::ConfigureMovement() const
{	
	auto Movement = GetCharacterMovement();
	Movement->bOrientRotationToMovement = true;
	Movement->RotationRate = FRotator(.0F, RotationRateYaw, .0F);
	Movement->JumpZVelocity = JumpZVelocity;
	Movement->AirControl = AirControlMeasure;	
}

void AMainCharacter::InitializeDefaultValues()
{
	// Stats
	SetMaxHealth(100.0F);
	SetHealth(65.0F);
	SetMinHealthForRecovery(15.0F);
	
	MaxStamina = 100.0F;
	Stamina = 10.0F;
	CoinCount = 0;
	WalkingSpeed = 140.0F;
	RunningSpeed = 650.0F;
	SprintingSpeed = 950.0F;
	MinStaminaForRecovery = 21.0F;
	// Statuses
	HealthStatus = EHealthStatus::EHS_Normal;
	HealthRecoveryRate = 1.5F;
	MovementStatus = EMovementStatus::EMS_Normal;
	StaminaStatus = EStaminaStatus::ESS_Normal;	
	StaminaSprintDrainRate = 1.0F;
	StaminaRecoveryRate = .5F;
	StaminaRecoveryWalkingMultiplier = 1.2F;
	StaminaRecoveryStillMultiplier = 1.5F;
	MinSprintStamina = 20.0F;
	// Data for camera and movement calculations
	CameraBoomLength = 600.F;
	CharacterCapsuleHalfHeight = 105.F;
	CharacterCapsuleWidth = 48.F;
	RotationRateYaw = 540.F;
	JumpZVelocity = 650.F;
	AirControlMeasure = .2F;	
	BaseTurnRate = 65.F;
	BaseLookUpRate = 65.F;
	bIsSprintKeyDown = false;
	bIsWalkSlowKeyDown = false;
	bIsLMBDown = false;
	bIsPauseButtonDown = false;	
	bIsMovingForward = false;
	bIsMovingRight = false;
}

void AMainCharacter::InitializeUserSettings()
{
	const auto GeneralUserSettings = GEngine->GetGameUserSettings();

	if (IsValid(GeneralUserSettings))
	{
		const auto UserSettings = Cast<UUGDCGameUserSettings>(GeneralUserSettings);

		if (IsValid(UserSettings))
		{
			bIsGamePadUsed = UserSettings->IsGamePadUsed();
		}		
	}
}

// Called when the game starts or when spawned
void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	PlayerController = Cast<AMainPlayerController>(GetController());	

	auto Map = GetWorld()->GetMapName();
	Map.RemoveFromStart(GetWorld()->StreamingLevelsPrefix);

	if (Map != "SunTemple")
	{
		LoadGameData(false, false);
		if (IsValid(PlayerController))
		{
			PlayerController->SetInputGameModeOnly();
		}
	}	
}

// Called every frame
void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!IsAlive()) return;
	
	UpdateHealthStatus(DeltaTime);
	UpdateStaminaStatus(DeltaTime);

	SetCombatTargetLocation();

	const auto HorizontalAxisInputValue = GetInputAxisValue("TurnAtRate");
	ULogger::LogWarning(FString::SanitizeFloat(HorizontalAxisInputValue));
	
}

void AMainCharacter::UpdateHealthStatus(const float& DeltaTime)
{
	const auto DeltaHealth = HealthRecoveryRate * DeltaTime;
	const auto CurrentHealthValue = GetHealth();
	const auto HealthValueForRecovery = GetMinHealthForRecovery();
	
	if (CurrentHealthValue < HealthValueForRecovery)
	{
		HealthStatus = EHealthStatus::EHS_BelowMinimum;
	}

	if (HealthStatus == EHealthStatus::EHS_BelowMinimum)
	{
		
		SetHealth(CurrentHealthValue + DeltaHealth);

		if (CurrentHealthValue >= HealthValueForRecovery)
		{
			SetHealth(HealthValueForRecovery);
			HealthStatus = EHealthStatus::EHS_Normal;
		}
	}
}

void AMainCharacter::UpdateStaminaStatus(const float& DeltaTime)
{
	const auto DeltaStaminaSpent = StaminaSprintDrainRate * DeltaTime;
	const auto DeltaStaminaRecovered = StaminaRecoveryRate * DeltaTime;

	const auto MovementData = GetCharacterMovement();

	if (IsValid(MovementData) && MovementData->IsFalling())
	{
		return;
	}
	
	switch (StaminaStatus)
	{
	case EStaminaStatus::ESS_Normal:
		if (bIsSprintKeyDown)
		{
			if ((Stamina - DeltaStaminaSpent) <= MinSprintStamina)
			{
				StaminaStatus = EStaminaStatus::ESS_BelowMinimum;
			}

			if (bIsMovingForward || bIsMovingRight)
			{
				Stamina -= DeltaStaminaSpent;
				SetMovementStatus(EMovementStatus::EMS_Sprinting);
			}
			else
			{
				SetMovementStatus(EMovementStatus::EMS_Normal);
			}
		}
		else
		{
			if (Stamina < MinStaminaForRecovery)
			{
				Stamina += DeltaStaminaRecovered;
			}
			
			if (Stamina >= MaxStamina)
			{
				Stamina = MaxStamina;
			}
			
			if (bIsWalkSlowKeyDown)
			{
				SetMovementStatus(EMovementStatus::EMS_Walking);
			}
			else
			{
				SetMovementStatus(EMovementStatus::EMS_Normal);
			}
		}

		break;
	case EStaminaStatus::ESS_BelowMinimum:
		if (bIsSprintKeyDown)
		{
			if (Stamina <= .0F)
			{
				StaminaStatus = EStaminaStatus::ESS_Exhausted;
				Stamina = .0F;
				SetMovementStatus(EMovementStatus::EMS_Normal);
			}
			else
			{
				if (bIsMovingForward || bIsMovingRight)
				{
					Stamina -= DeltaStaminaSpent;
					SetMovementStatus(EMovementStatus::EMS_Sprinting);
				}
				else
				{
					SetMovementStatus(EMovementStatus::EMS_Normal);
				}
			}
		}
		else
		{
			if (Stamina < MinStaminaForRecovery)
			{
				Stamina += DeltaStaminaRecovered;
			}

			if (Stamina >= MinSprintStamina)
			{
				StaminaStatus = EStaminaStatus::ESS_Normal;
			}
			
			CheckWalkingStatus();
		}

		break;
	case EStaminaStatus::ESS_Exhausted:
		if (bIsSprintKeyDown)
		{
			Stamina = .0F;
		}
		else
		{
			StaminaStatus = EStaminaStatus::ESS_ExhaustedRecovering;
			if (Stamina < MinStaminaForRecovery)
			{
				Stamina += DeltaStaminaRecovered;
			}
		}
		
		CheckWalkingStatus();
		break;
	case EStaminaStatus::ESS_ExhaustedRecovering:
		if (Stamina < MinStaminaForRecovery)
		{
			Stamina += DeltaStaminaRecovered;
		}

		if (Stamina >= MinSprintStamina)
		{
			StaminaStatus = EStaminaStatus::ESS_Normal;
		}
		
		CheckWalkingStatus();
		break;
	default:
		break;
	}
}

void AMainCharacter::SetCombatTargetLocation()
{
	const auto CurrentCombatTarget = GetCombatTarget();
	if (!IsValid(CurrentCombatTarget)) return;

	CombatTargetLocation = CurrentCombatTarget->GetActorLocation();

	if (!IsValid(PlayerController)) return;

	PlayerController->SetEnemyLocation(CombatTargetLocation);
}

void AMainCharacter::CheckWalkingStatus()
{
	if (bIsWalkSlowKeyDown)
	{
		SetMovementStatus(EMovementStatus::EMS_Walking);
	}
	else
	{
		SetMovementStatus(EMovementStatus::EMS_Normal);
	}
}

// Called to bind functionality to input
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AMainCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &AMainCharacter::SprintKeyDown);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &AMainCharacter::SprintKeyUp);

	PlayerInputComponent->BindAction("WalkSlow", IE_Pressed, this, &AMainCharacter::WalkSlowKeyDown);
	PlayerInputComponent->BindAction("WalkSlow", IE_Released, this, &AMainCharacter::WalkSlowKeyUp);

	PlayerInputComponent->BindAction("LMB", IE_Pressed, this, &AMainCharacter::LMBDown);
	PlayerInputComponent->BindAction("LMB", IE_Released, this, &AMainCharacter::LMBUp);

	PlayerInputComponent->BindAction("Use", IE_Pressed, this, &AMainCharacter::UseKeyDown);

	PlayerInputComponent->BindAction("CenterCamera", IE_Pressed, this, &AMainCharacter::OnCameraRotationReset);

	FInputActionBinding& PauseButtonDown = PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &AMainCharacter::PauseButtonDown);
	PauseButtonDown.bExecuteWhenPaused = true;	

	PlayerInputComponent->BindAxis("MoveForward", this, &AMainCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMainCharacter::MoveRight);

	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("TurnAtRate", this, &AMainCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUpAtRate", this, &AMainCharacter::LookUpAtRate);	
}

/* Called for forwards/backwards input */
void AMainCharacter::MoveForward(float MovementValue)
{
	MoveInAxis(MovementValue, EAxis::X);	
}

/* Called for side to side input */
void AMainCharacter::MoveRight(float Value)
{
	MoveInAxis(Value, EAxis::Y);
}

void AMainCharacter::MoveInAxis(const float& MovementValue, const EAxis::Type& Axis)
{
	if (Axis == EAxis::Y)
	{
		bIsMovingRight = false;
	}
	if (Axis == EAxis::X)
	{
		bIsMovingForward = false;
	}
		
	if (!IsAlive()) return;
	
	if ((!IsValid(Controller)) || (MovementValue == .0F)) return;

	// Get forward orientation
	const auto Rotation = Controller->GetControlRotation();
	const auto YawRotation = FRotator(.0f, Rotation.Yaw, .0F);

	// Get direction based on rotation matrix - an axis direction of
	// object's translation when object is in current rotation
	const auto Direction = FRotationMatrix(YawRotation).GetUnitAxis(Axis);
	AddMovementInput(Direction, MovementValue);

	if (Axis == EAxis::Y)
	{
		bIsMovingRight = true;
	}
	if (Axis == EAxis::X)
	{
		bIsMovingForward = true;		
		ForceCenterCamera();
	}
}

void AMainCharacter::ForceCenterCamera()
{
	const auto HorizontalAxisGamePadInputValue = GetInputAxisValue("TurnAtRate");
	
	if (UKismetMathLibrary::Abs(HorizontalAxisGamePadInputValue) < .000001F && bIsGamePadUsed)
	{
		OnCameraRotationReset();
	}
}

void AMainCharacter::TurnAtRate(float Rate)
{
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMainCharacter::LookUpAtRate(float Rate)
{
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AMainCharacter::Die(AActor* DeathCauser)
{
	Super::Die(DeathCauser);

	if (!IsAlive()) return;

	auto AnimInstance = GetMesh()->GetAnimInstance();
	const auto Montage = GetCombatMontage();

	if (!IsValid(Montage) || !IsValid(AnimInstance)) return;	
	
	const auto SectionNumber = FMath::RandRange(0, 1);
	switch (SectionNumber)
	{
	case 0:
		AnimInstance->Montage_Play(Montage, 1.0F);
		AnimInstance->Montage_JumpToSection(FName("Death_01"), Montage);
		break;
	case 1:
		AnimInstance->Montage_Play(Montage, 1.75F);
		AnimInstance->Montage_JumpToSection(FName("Death_02"), Montage);
		break; 
	default:
		break;
	}

	SetMovementStatus(EMovementStatus::EMS_Dead);
}

void AMainCharacter::DeathEnd()
{
	GetMesh()->bPauseAnims = true;
	GetMesh()->bNoSkeletonUpdate = true;
}

void AMainCharacter::IncrementCoins(const int32& Amount)
{
	CoinCount += Amount;
}

void AMainCharacter::IncrementStamina(const float& Amount)
{
	Stamina += Amount;
	if (Stamina > MaxStamina)
	{
		Stamina = MaxStamina;
	}
}

void AMainCharacter::SetMovementStatus(EMovementStatus Status)
{
	MovementStatus = Status;
	const auto Movement = GetCharacterMovement();

	switch (Status)
	{
	case EMovementStatus::EMS_Normal:
		Movement->MaxWalkSpeed = RunningSpeed;
		break;
	case EMovementStatus::EMS_Walking:
		Movement->MaxWalkSpeed = WalkingSpeed;
		break;
	case EMovementStatus::EMS_Sprinting:
		Movement->MaxWalkSpeed = SprintingSpeed;
		break;
	default:
		break;
	}
}

void AMainCharacter::SprintKeyDown()
{
	bIsSprintKeyDown = true;
}

void AMainCharacter::SprintKeyUp()
{
	bIsSprintKeyDown = false;
}

void AMainCharacter::WalkSlowKeyDown()
{
	bIsWalkSlowKeyDown = true;
}

void AMainCharacter::WalkSlowKeyUp()
{
	bIsWalkSlowKeyDown = false;
}

void AMainCharacter::LMBDown()
{
	bIsLMBDown = true;

	if (!IsAlive()) return;

	if (IsValid(PlayerController) && (PlayerController->IsPauseMenuVisible())) return;

	if (IsValid(EquippedWeapon))
	{
		AttackStart();		
	}
}

void AMainCharacter::LMBUp()
{
	bIsLMBDown = false;
}

void AMainCharacter::UseKeyDown()
{
	if (IsValid(ActiveOverlappingItem))
	{
		auto InteractableObject = Cast<IInteractInterface>(ActiveOverlappingItem);

		if (InteractableObject)
		{
			InteractableObject->OnInteract(this);
		}		

		SetActiveOverlappingItem(nullptr);
	}
}


void AMainCharacter::PauseButtonDown()
{
	bIsPauseButtonDown = true;

	if (IsValid(PlayerController))
	{
		PlayerController->TogglePauseMenu();
	}
}

void AMainCharacter::CenterCameraButtonDown()
{
	OnCameraRotationReset();
}

void AMainCharacter::SetEquippedWeapon(AWeapon* WeaponToEquip)
{
	if (IsValid(EquippedWeapon))
	{
		EquippedWeapon->Destroy();		
	}
	
	EquippedWeapon = WeaponToEquip;
}

void AMainCharacter::AttackStart()
{
	if (!IsAlive()) return;
	
	if (IsAttacking()) return;
	
	SetIsAttacking(true);
	SetTurnToEnemyState(true);
	
	auto AnimInstance = GetMesh()->GetAnimInstance();
	const auto Montage = GetCombatMontage();	

	if (!IsValid(AnimInstance) || !IsValid(Montage)) return;

	const auto SectionNumber = FMath::RandRange(0, 1);
	switch (SectionNumber)
	{
	case 0:
		AnimInstance->Montage_Play(Montage, 1.35F);
		AnimInstance->Montage_JumpToSection(FName("Attack_01"), Montage);
		break;
	case 1:
		AnimInstance->Montage_Play(Montage, 1.75F);
		AnimInstance->Montage_JumpToSection(FName("Attack_02"), Montage);
		break;
	default:
		break;
	}
}

void AMainCharacter::AttackEnd()
{
	SetIsAttacking(false);
	SetTurnToEnemyState(false);
	if (bIsLMBDown)
	{
		AttackStart();
	}
}

void AMainCharacter::PlayWeaponAttackSound() const
{
	if (IsValid(EquippedWeapon))
	{
		EquippedWeapon->PlayAttackSound();
	}
}

bool AMainCharacter::IsAlive() const
{
	return MovementStatus != EMovementStatus::EMS_Dead;
}

void AMainCharacter::Jump()
{
	if (!IsAlive()) return;

	Super::Jump();
}

void AMainCharacter::UpdateClosestCombatTarget()
{
	auto OverlappingActorS = TArray<AActor*>();
	GetOverlappingActors(OverlappingActorS, CombatTargetClassFilter);

	if (OverlappingActorS.Num() == 0)
	{
		if (IsValid(PlayerController))
		{
			PlayerController->HideEnemyHealthBar();
		}

		return;
	};

	auto ClosestEnemy = Cast<AEnemy>(OverlappingActorS[0]);
	
	if (!IsValid(ClosestEnemy)) return;

	const auto PlayerLocation = GetActorLocation();
	auto MinDistanceToEnemy = (ClosestEnemy->GetActorLocation() - PlayerLocation).Size();

	for (const auto& Actor : OverlappingActorS)
	{
		const auto Enemy = Cast<AEnemy>(Actor);

		if (!IsValid(Enemy)) continue;
		
		const auto DistanceToActor = (Enemy->GetActorLocation() - PlayerLocation).Size();
		if (DistanceToActor < MinDistanceToEnemy)
		{
			MinDistanceToEnemy = DistanceToActor;
			ClosestEnemy = Enemy;
		}
	}

	if (IsValid(PlayerController))
	{
		PlayerController->DisplayEnemyHealthBar();
	}
	
	SetCombatTarget(ClosestEnemy);
}

void AMainCharacter::SaveGameData() const
{
	auto SaveGameInstance = Cast<UUGDCSaveGame>(UGameplayStatics::CreateSaveGameObject(UUGDCSaveGame::StaticClass()));

	if (!IsValid(SaveGameInstance)) return;

	auto DataToSave = new FSaveGameData();
	
	DataToSave->Health = GetHealth();
	DataToSave->MaxHealth = GetMaxHealth();
	DataToSave->Stamina = Stamina;
	DataToSave->MaxStamina = MaxStamina;
	DataToSave->CoinCount = CoinCount;
	DataToSave->PlayerLocation = GetActorLocation();
	DataToSave->PlayerRotation = GetActorRotation();

	const auto World = GetWorld();

	if (IsValid(World))
	{
		auto MapName = World->GetMapName();
		MapName.RemoveFromStart(World->StreamingLevelsPrefix);
		DataToSave->LevelName = MapName;
	}

	if (IsValid(EquippedWeapon))
	{
		DataToSave->EquippedWeaponName = EquippedWeapon->GetItemName();
	}

	SaveGameInstance->SetSaveData(DataToSave);
	delete DataToSave;

	UGameplayStatics::SaveGameToSlot(SaveGameInstance, SaveGameInstance->GetPlayerName(), SaveGameInstance->GetUserIndex());
}
 
void AMainCharacter::LoadGameData(bool IsSetPositionNeeded, bool IsLevelSwitchNeeded)
{
	auto LoadGameInstance = Cast<UUGDCSaveGame>(UGameplayStatics::CreateSaveGameObject(UUGDCSaveGame::StaticClass()));

	if (!IsValid(LoadGameInstance)) return;

	LoadGameInstance = Cast<UUGDCSaveGame>(UGameplayStatics::LoadGameFromSlot(LoadGameInstance->GetPlayerName(), LoadGameInstance->GetUserIndex()));

	if (!IsValid(LoadGameInstance)) return;

	const auto LoadedData = LoadGameInstance->GetSaveGameData();

	SetHealth(LoadedData.Health);
	SetMaxHealth(LoadedData.MaxHealth);
	Stamina = LoadedData.Stamina;
	MaxStamina = LoadedData.MaxStamina;
	CoinCount = LoadedData.CoinCount;

	auto World = GetWorld();

	if (IsValid(World))
	{
		const auto Storage = World->SpawnActor<AItemStorage>(ItemStorage);
		if (IsValid(Storage))
		{
			const auto EquippedWeaponName = LoadedData.EquippedWeaponName;

			if (!EquippedWeaponName.IsEmpty())
			{
				const auto StorageItems = Storage->GetItems();				
				const auto StoredEquippedWeapon = StorageItems[EquippedWeaponName];

				if (!IsValid(StoredEquippedWeapon))
				{
					ULogger::LogWarning("MainCharacter. LoadGameData. Can't find equipped weapon in the Storage. Please check the Storage to match the actual weapons in game");
				}
								
				auto WeaponToEquip = World->SpawnActor<AWeapon>(StoredEquippedWeapon);
				WeaponToEquip->OnInteract(this);
			}
		}
	}

	if (IsLevelSwitchNeeded && IsSetPositionNeeded)
	{
		SetActorLocation(LoadedData.PlayerLocation);
		SetActorRotation(LoadedData.PlayerRotation);
	}

	SetMovementStatus(EMovementStatus::EMS_Normal);
	GetMesh()->bPauseAnims = false;
	GetMesh()->bNoSkeletonUpdate = false;

	if (IsLevelSwitchNeeded)
	{
		if (!LoadedData.LevelName.IsEmpty())
		{
			auto LevelName = FName(*(LoadedData.LevelName));
			SwitchLevel(LevelName);
		}
	}
}

void AMainCharacter::SwitchLevel(FName LevelName) const
{
	const auto World = GetWorld();

	if (!IsValid(World)) return;
	
	const auto CurrentLevel = World->GetMapName();
	const auto CurrentLevelName = FName(*CurrentLevel);

	if (CurrentLevelName != LevelName)
	{
		UGameplayStatics::OpenLevel(World, LevelName);
	}
}

void AMainCharacter::ShowPickupLocations()
{
	for (const auto& Location : PickupLocations)
	{
		UKismetSystemLibrary::DrawDebugSphere
		(
			this,
			Location + FVector(.0F, .0F, 75.0F),
			25.0F,
			12,
			FLinearColor::Green,
			5.0F,
			3.0F
			);
	}
}
