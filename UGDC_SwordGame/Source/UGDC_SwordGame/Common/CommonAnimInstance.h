// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "CommonAnimInstance.generated.h"

/**
 * Base class for characters' animation instances used in the game
 */
UCLASS(Abstract)
class UGDC_SWORDGAME_API UCommonAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:	
	UCommonAnimInstance();

	virtual void NativeInitializeAnimation() override;

protected:
	/* An analogue of Tick() function */
	UFUNCTION(BlueprintCallable, Category = AnimationProperties)
	virtual void UpdateAnimationProperties();

	FORCEINLINE class APawn* GetCharacterPawn() const { return CharacterPawn; }
	
	FORCEINLINE float GetMovementSpeed() const { return MovementSpeed; }	
	FORCEINLINE void SetMovementSpeed(const float& SpeedToSet) { MovementSpeed = SpeedToSet; }

	FORCEINLINE bool IsInAir() const { return bIsInAir; }
	FORCEINLINE void SetInAir(const bool& bIsCurrentlyInAir) { bIsInAir = bIsCurrentlyInAir; }
	
private:
	/* The pawn component of a character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	APawn* CharacterPawn;
	
	/* Current movement speed of a character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float MovementSpeed;

	/* Is character currently in air */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bIsInAir;
	
	/* Set character pawn from pawn owner*/
	void SetCharacterPawn();
};
