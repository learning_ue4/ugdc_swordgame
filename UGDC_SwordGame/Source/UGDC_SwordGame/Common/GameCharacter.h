// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameCharacter.generated.h"

/*
 * Base class for game character - player or NPC
 */
UCLASS(Abstract)
class UGDC_SWORDGAME_API AGameCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AGameCharacter();

	/* Play particle effect when being hit at certain location */
	void PlayHitReceiveEffect(const FVector& Location) const;

	/* Play sound when being hit */
	void PlayTakeHitSound() const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FORCEINLINE void SetHealth(const float& HealthToSet) { Health = HealthToSet; }
	FORCEINLINE void SetMaxHealth(const float& MaxHealthToSet) { MaxHealth = MaxHealthToSet; }	
	FORCEINLINE void SetMinHealthForRecovery(const float& MinHealthForRecoveryToSet) { MinHealthForRecovery = MinHealthForRecoveryToSet; }

	FORCEINLINE void SetTurnToEnemySpeed(const float& SpeedToSet) { TurnToEnemySpeed = SpeedToSet; }
	FORCEINLINE float GetTurnToEnemySpeed() const { return TurnToEnemySpeed; }
	
	FORCEINLINE void SetTurnToEnemyState(const bool& bIsCurrentlyTurnedToEnemy) { bIsTurnedToEnemy = bIsCurrentlyTurnedToEnemy; }
	FORCEINLINE bool IsTurnedToEnemy() const { return bIsTurnedToEnemy; }

	FORCEINLINE UAnimMontage* GetCombatMontage() const { return CombatMontage; }

	/* Set player rotation to face combat target */
	void RotateToCombatTarget(const float& DeltaTime);

	FORCEINLINE bool IsAttacking() const { return bIsAttacking; }

	UFUNCTION(BlueprintCallable, Category = Combat)
	void SetIsAttacking(bool bIsCurrentlyAttacking) { bIsAttacking = bIsCurrentlyAttacking; }
	
	/* Pass away */
	virtual void Die(AActor* DeathCauser);

	/* Init default values */
	void InitDefaults();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/* Go into attacking state and perform an attack */
	virtual void AttackStart();

	/* Take damage */
	virtual float TakeDamage
	(
		float DamageAmount,
		struct FDamageEvent const& DamageEvent,
		class AController* EventInstigator,
		AActor* DamageCauser
	) override;

	UFUNCTION(BlueprintCallable, Category = PlayerStats)
	FORCEINLINE float GetMaxHealth() const { return MaxHealth; }
	
	UFUNCTION(BlueprintCallable, Category = PlayerStats)
	FORCEINLINE float GetHealth() const { return Health; }	
	
	UFUNCTION(BlueprintCallable, Category = PlayerStats)
	FORCEINLINE float GetMinHealthForRecovery() const { return MinHealthForRecovery; }
	
	/* Increment health by a certain value */
	UFUNCTION(BlueprintCallable, Category = Stats)
	void IncrementHealth(const float& Amount);

	FORCEINLINE AGameCharacter* GetCombatTarget() const { return CombatTarget; }

	FORCEINLINE void SetCombatTarget(class AGameCharacter* Target)
	{
		CombatTarget = Target;
		bHasCombatTarget = (Target != nullptr);
	}

	/* Set the fact that player doesn't have a combat target */
	FORCEINLINE void SetHasNoCombatTarget() { bHasCombatTarget = false; }

	UFUNCTION(BlueprintCallable, Category = Combat)
	FORCEINLINE bool HasCombatTarget() const { return bHasCombatTarget; }

private:
	/* Health stats  - to be defined in derived classes*/
	UPROPERTY(VisibleAnywhere, Category = PlayerStats, meta = (AllowPrivateAccess = "true"))
	float MaxHealth;
	UPROPERTY(VisibleAnywhere, Category = PlayerStats, meta = (AllowPrivateAccess = "true"))
	float Health;
	/* Minimum health required for starting health recovery */
	float MinHealthForRecovery;

	/* Speed at which player turns to enemy's direction when attacking */
	UPROPERTY(EditDefaultsOnly, Category = TechStats, meta = (AllowPrivateAccess = "true"))
	float TurnToEnemySpeed;

	/* Is player currently turned at enemy's direction */
	bool bIsTurnedToEnemy;

	/* Current character's combat target */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	AGameCharacter* CombatTarget;

	/* Animation montage used for combat stuff (attack, take damage, death, etc.) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Animation, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* CombatMontage;

	/* Is performing an attack at the moment */
	UPROPERTY(VisibleAnywhere, Category = Combat, meta = (AllowPrivateAccess = "true"))
	bool bIsAttacking;

	/* Do we have a combat target now? (sets with setting the combat target, made for using in BP to simplify logic) */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	bool bHasCombatTarget;

	/* Particles played when enemy is being hit */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	class UParticleSystem* HitReceivedParticleSystem;
	
	/* Sound played when being hit */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	class USoundCue* TakeHitSound;	
};
