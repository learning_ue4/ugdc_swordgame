// Fill out your copyright notice in the Description page of Project Settings.


#include "GameCharacter.h"
#include "Components/SkeletalMeshComponent.h"
#include "UGDC_SwordGame/NPC/Enemy.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Sound/SoundCue.h"
#include "Kismet/KismetMathLibrary.h"

AGameCharacter::AGameCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void AGameCharacter::InitDefaults()
{
	TurnToEnemySpeed = 15.0F;
	bIsTurnedToEnemy = false;
	
	TakeHitSound = nullptr;
	HitReceivedParticleSystem = nullptr;
	CombatMontage = nullptr;
	CombatTarget = nullptr;
}

// Called when the game starts or when spawned
void AGameCharacter::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AGameCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsTurnedToEnemy())
	{
		RotateToCombatTarget(DeltaTime);
	}
}

// Called to bind functionality to input
void AGameCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AGameCharacter::RotateToCombatTarget(const float& DeltaTime)
{
	const auto CurrentCombatTarget = GetCombatTarget();
	if (!IsValid(CurrentCombatTarget)) return;

	const auto LookAtRotation = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), CurrentCombatTarget->GetActorLocation());
	const auto RotationToTarget = FRotator(LookAtRotation.Pitch, LookAtRotation.Yaw, .0F);
	const auto InterpRotation = FMath::RInterpTo(GetActorRotation(), RotationToTarget, DeltaTime, TurnToEnemySpeed);

	SetActorRotation(InterpRotation);
}

void AGameCharacter::AttackStart() { }

float AGameCharacter::TakeDamage
(
	float DamageAmount,
	struct FDamageEvent const& DamageEvent,
	class AController* EventInstigator,
	AActor* DamageCauser
)
{
	Health -= DamageAmount;

	if (Health <= 0.0F)
	{
		Die(DamageCauser);
	}

	return DamageAmount;
}

void AGameCharacter::IncrementHealth(const float& Amount)
{
	Health += Amount;
	
	if (Health > MaxHealth) Health = MaxHealth;
}

void AGameCharacter::Die(AActor* DeathCauser)
{
	auto Enemy = Cast<AEnemy>(DeathCauser);

	if (IsValid(Enemy))
	{
		Enemy->SetHasNoCombatTarget();
	}
}

void AGameCharacter::PlayHitReceiveEffect(const FVector& Location) const
{
	if (!IsValid(HitReceivedParticleSystem)) return;
	
	const auto World = GetWorld();
	if (!IsValid(World)) return;
		
	UGameplayStatics::SpawnEmitterAtLocation(World, HitReceivedParticleSystem, Location);
}

void AGameCharacter::PlayTakeHitSound() const
{	
	if ((TakeHitSound->IsPlayable()))
	{
		UGameplayStatics::PlaySound2D(this, TakeHitSound);
	}
}
