// Fill out your copyright notice in the Description page of Project Settings.


#include "CommonAnimInstance.h"

UCommonAnimInstance::UCommonAnimInstance()
{
	CharacterPawn = nullptr;
}

void UCommonAnimInstance::NativeInitializeAnimation()
{
	SetCharacterPawn();
}

void UCommonAnimInstance::UpdateAnimationProperties()
{
	SetCharacterPawn();
}

void UCommonAnimInstance::SetCharacterPawn()
{
	if (CharacterPawn == nullptr)
	{
		CharacterPawn = TryGetPawnOwner();
	}
}