// Fill out your copyright notice in the Description page of Project Settings.

#include "SpawnVolume.h"
#include "Components/BoxComponent.h"
#include "AIController.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "UGDC_SwordGame/Critter.h"
#include "UGDC_SwordGame/NPC/Enemy.h"
#include "Logger.h"

// Sets default values
ASpawnVolume::ASpawnVolume()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpawningBox = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawningBox"));
}

// Called when the game starts or when spawned
void ASpawnVolume::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ASpawnVolume::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);	
}

FVector ASpawnVolume::GetSpawnPoint() const
{
	const auto Extent = SpawningBox->GetScaledBoxExtent();
	const auto Origin = SpawningBox->GetComponentLocation();
	const auto Point = UKismetMathLibrary::RandomPointInBoundingBox(Origin, Extent);

	return Point;
}

TSubclassOf<AActor> ASpawnVolume::GetSpawnActor() const
{
	if (ActorsToSpawn.Num() < 1) return nullptr;

	const auto SelectionIndex = FMath::RandRange(0, ActorsToSpawn.Num() - 1);
	return ActorsToSpawn[SelectionIndex];
}

void ASpawnVolume::SpawnActor_Implementation(UClass* ToSpawn, const FVector& Location)
{
	if (!IsValid(ToSpawn)) return;

	auto World = GetWorld();
	if (!IsValid(World)) return;
	
	const FActorSpawnParameters SpawnParameters;
	const auto Spawned = World->SpawnActor<AActor>(ToSpawn, Location, FRotator(.0F), SpawnParameters);
	if (!IsValid(Spawned))
	{
		ULogger::LogWarning("SpawnVolume. SpawnActor. Cannot spawn proper actor, check UClass to spawn");
		return;
	}

	auto Enemy = Cast<AEnemy>(Spawned);
	if (!IsValid(Enemy)) return;

	Enemy->SpawnDefaultController(); // make a default AI controller for the enemy

	const auto AICtrl = Cast<AAIController>(Enemy->GetController());
	if (!IsValid(AICtrl)) return;

	Enemy->SetAIController(AICtrl);
}
