// Fill out your copyright notice in the Description page of Project Settings.

#include "Floater.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
AFloater::AFloater()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CustomStaticMesh"));

	InitialLocation = FVector(0.0F);
	PlacedLocation = FVector(0.0F);
	WorldOrigin = FVector(0.0F, 0.0F, 0.0F);
	InitialDirection = FVector(0.0F, 0.0F, 0.0F);
	InitialForce = FVector(20000000.0F, .0F, .0F);
	InitialTorque = FVector(20000000.0F, .0F, .0F);
	
	bIsInitializeFloaterLocationNeeded = false;
	bShouldFloat = false;

	Amplitude = 1.0F;
	TimeStretch = 1.0F;

	RunningTime = 0.F;
}

// Called when the game starts or when spawned
void AFloater::BeginPlay()
{
	Super::BeginPlay();

	auto InitialX = FMath::FRandRange(-500.F, 500.F);
	auto InitialY = FMath::FRandRange(-500.F, 500.F);
	auto InitialZ = FMath::FRandRange(0.F, 500.F);

	InitialLocation.X = InitialX;
	InitialLocation.Y = InitialY;
	InitialLocation.Z = InitialZ;	
	
	PlacedLocation = GetActorLocation();

	if (bIsInitializeFloaterLocationNeeded)
	{
		SetActorLocation(InitialLocation);
	}	
}

// Called every frame
void AFloater::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if (bShouldFloat)
	{
		FVector NewLocation = GetActorLocation();

		NewLocation.Z = NewLocation.Z + Amplitude * FMath::Sin(TimeStretch * RunningTime);
		NewLocation.Y = NewLocation.Y + Amplitude * FMath::Cos(TimeStretch * RunningTime);
		
		SetActorLocation(NewLocation);
		RunningTime += DeltaTime;
	}
}
