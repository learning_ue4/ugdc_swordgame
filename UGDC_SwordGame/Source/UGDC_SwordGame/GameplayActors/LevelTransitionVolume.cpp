// Fill out your copyright notice in the Description page of Project Settings.


#include "LevelTransitionVolume.h"
#include "Components/BoxComponent.h"
#include "Components/BillboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "UGDC_SwordGame/Player/MainCharacter.h"

// Sets default values
ALevelTransitionVolume::ALevelTransitionVolume()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	TransitionVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("TransitionVolume"));
	RootComponent = TransitionVolume;

	BillboardComponent = CreateDefaultSubobject<UBillboardComponent>(TEXT("Billboard"));
	BillboardComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ALevelTransitionVolume::BeginPlay()
{
	Super::BeginPlay();

	TransitionVolume->OnComponentBeginOverlap.AddDynamic(this, &ALevelTransitionVolume::OnOverlapBegin);
}

// Called every frame
void ALevelTransitionVolume::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ALevelTransitionVolume::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
                                            AActor* OtherActor,
                                            UPrimitiveComponent* OtherComp,
                                            int32 OtherBodyIndex,
                                            bool bFromSweep,
                                            const FHitResult& SweepResult)
{
	if (!IsValid(OtherActor)) return;

	const auto MainCharacter = Cast<AMainCharacter>(OtherActor);

	if (!IsValid(MainCharacter)) return;

	TransitionToLevel(OtherActor);
}

void ALevelTransitionVolume::TransitionToLevel(AActor* ActorToTransit) const
{
	if (TransitionLevelName.GetStringLength() < 1) return;

	if (!IsValid(ActorToTransit)) return;

	const auto MainCharacter = Cast<AMainCharacter>(ActorToTransit);
	if (!IsValid(MainCharacter)) return;

	MainCharacter->SwitchLevel(TransitionLevelName);
}
