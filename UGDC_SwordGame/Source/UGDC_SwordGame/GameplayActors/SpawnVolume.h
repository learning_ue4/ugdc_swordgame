// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SpawnVolume.generated.h"

/*
 * Spawning pool for critters: a rectangular-shaped volume where critters are spawned.	
 * Spawning occurs at random points, objects to spawn are selected from list.
 */
UCLASS()
class UGDC_SWORDGAME_API ASpawnVolume : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawnVolume();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/* Blueprint-implemented spawning function */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = Spawning)
	void SpawnActor(UClass* ToSpawn, const FVector& Location);

private:
	/* Spawn volume's borders */
	UPROPERTY(VisibleAnywhere, Category = Spawning, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* SpawningBox;

	/* Objects to spawn at this volume */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = Spawning, meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<AActor>> ActorsToSpawn;

	/* A randomly generated point in a box where a unit will be spawned */
	UFUNCTION(BlueprintPure, Category = Spawning, meta = (AllowPrivateAccess = "true"))
	FVector GetSpawnPoint() const;

	/* A random actor from spawn array */
	UFUNCTION(BlueprintPure, Category = Spawning, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AActor> GetSpawnActor() const;
};
