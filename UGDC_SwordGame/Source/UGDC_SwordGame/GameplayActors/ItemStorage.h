// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ItemStorage.generated.h"

UCLASS()
class UGDC_SWORDGAME_API AItemStorage : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItemStorage();

	FORCEINLINE TMap<FString, TSubclassOf<class AItem>> GetItems() const { return Items; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	/* Stored items */
	UPROPERTY(EditDefaultsOnly, Category = SavedItems, meta = (AllowPrivateAccess = "true"))
	TMap<FString, TSubclassOf<class AItem>> Items;	
};
