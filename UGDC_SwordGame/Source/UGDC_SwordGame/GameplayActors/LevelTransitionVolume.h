// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LevelTransitionVolume.generated.h"

UCLASS()
class UGDC_SWORDGAME_API ALevelTransitionVolume final : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALevelTransitionVolume();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	/* A rectangular volume which should transition the player to the level upon overlapping */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Transition, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* TransitionVolume;

	/* A billboard for the editor */
	class UBillboardComponent* BillboardComponent;

	/* Name of the level where we want to go */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Transition, meta = (AllowPrivateAccess = "true"))
	FName TransitionLevelName;
	
	/* Perform a transition to a particular level */
	void TransitionToLevel(AActor* ActorToTransit) const;

	/* Events occured on start colliding (overlapping) with item */
	UFUNCTION()
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                            AActor* OtherActor,
	                            UPrimitiveComponent* OtherComp,
	                            int32 OtherBodyIndex,
	                            bool bFromSweep,
	                            const FHitResult& SweepResult);
};
