// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "UGDCSaveGame.generated.h"

/*
 * Struct that holds all necessary game data
 * to transit it through levels
 */
USTRUCT(BlueprintType)
struct FSaveGameData
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, Category = SaveGameData)
	float Health;

	UPROPERTY(VisibleAnywhere, Category = SaveGameData)
	float MaxHealth;

	UPROPERTY(VisibleAnywhere, Category = SaveGameData)
	float Stamina;

	UPROPERTY(VisibleAnywhere, Category = SaveGameData)
	float MaxStamina;

	UPROPERTY(VisibleAnywhere, Category = SaveGameData)
	int32 CoinCount;

	UPROPERTY(VisibleAnywhere, Category = SaveGameData)
	FVector PlayerLocation;

	UPROPERTY(VisibleAnywhere, Category = SaveGameData)
	FRotator PlayerRotation;

	UPROPERTY(VisibleAnywhere, Category = SaveGameData)
	FString EquippedWeaponName;

	UPROPERTY(VisibleAnywhere, Category = SaveGameData)
	FString LevelName;
};

/*
 * Game saver
 */
UCLASS()
class UGDC_SWORDGAME_API UUGDCSaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	UUGDCSaveGame();

	FORCEINLINE FString GetPlayerName() const { return PlayerName; }
	
	FORCEINLINE uint32 GetUserIndex() const { return UserIndex; }

	FORCEINLINE FSaveGameData GetSaveGameData() const { return SaveGameData; }
	FORCEINLINE void SetSaveData(FSaveGameData* DataToSet) { SaveGameData = *DataToSet; }	

private:
	/* Profile name */
	UPROPERTY(VisibleAnywhere, Category = Basic)
	FString PlayerName;

	/* ??? */
	UPROPERTY(VisibleAnywhere, Category = Basic)
	uint32 UserIndex;

	/* The actual data to save */
	UPROPERTY(VisibleAnywhere, Category = Basic)
	FSaveGameData SaveGameData;
};
