// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy.h"
#include "Components/SphereComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "AIController.h"
#include "UGDC_SwordGame/Player/MainCharacter.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/SkeletalMeshSocket.h"
#include "TimerManager.h"
#include "Sound/SoundCue.h"
#include "Logger.h"
#include <string>

// Sets default values
AEnemy::AEnemy()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InitializeDefaultValues();
	InitializeAI();
}

void AEnemy::InitializeDefaultValues()
{
	SetHealth(75.0);
	SetMaxHealth(100.0F);
	
	AggroSphereRadius = 600.0F;
	CombatSphereRadius = 120.0F;
	bIsCombatSphereOverlapped = false;
	DamageBase = 10.0F;
	DamageScatter = 5.0F;
	AttackDelayTimeMin = .15F;
	AttackDelayTimeMax = 1.0F;
	MovementStatus = EEnemyMovementStatus::EMS_Idle;
	AIController = nullptr;
	AttackSound = nullptr;
}

void AEnemy::InitializeAI()
{
	AggroSphere = CreateDefaultSubobject<USphereComponent>(TEXT("AggroSphere"));
	AggroSphere->SetupAttachment(GetRootComponent());
	AggroSphere->InitSphereRadius(AggroSphereRadius);

	CombatSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CombatSphere"));
	CombatSphere->SetupAttachment(GetRootComponent());
	CombatSphere->InitSphereRadius(CombatSphereRadius);
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	AIController = Cast<AAIController>(GetController());

	AggroSphere->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::OnAggroSphereOverlapBegin);
	AggroSphere->OnComponentEndOverlap.AddDynamic(this, &AEnemy::OnAggroSphereOverlapEnd);

	CombatSphere->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::OnCombatSphereOverlapBegin);
	CombatSphere->OnComponentEndOverlap.AddDynamic(this, &AEnemy::OnCombatSphereOverlapEnd);

	SetIgnoreCamera();
}

void AEnemy::SetIgnoreCamera()
{
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AEnemy::OnAggroSphereOverlapBegin(UPrimitiveComponent* OverlappedComponent,
                                       AActor* OtherActor,
                                       UPrimitiveComponent* OtherComp,
                                       int32 OtherBodyIndex,
                                       bool bFromSweep,
                                       const FHitResult& SweepResult)
{
	if (!IsAlive() || !IsValid(OtherActor)) return;	
	
	const auto TargetCharacter = Cast<AMainCharacter>(OtherActor);

	if (!IsValid(TargetCharacter)) return;

	MoveToTarget(TargetCharacter);
}

void AEnemy::OnAggroSphereOverlapEnd(UPrimitiveComponent* OverlappedComponent,
                                     AActor* OtherActor,
                                     UPrimitiveComponent* OtherComp,
                                     int32 OtherBodyIndex)
{
	if (!IsValid(OtherActor)) return;

	auto TargetCharacter = Cast<AMainCharacter>(OtherActor);

	if (!IsValid(TargetCharacter)) return;
	
	SetCombatTarget(nullptr);
	SetHasNoCombatTarget();

	if (TargetCharacter->GetCombatTarget() == this)
	{
		TargetCharacter->SetCombatTarget(nullptr);
	}
	TargetCharacter->SetHasNoCombatTarget();

	TargetCharacter->UpdateClosestCombatTarget();

	StopMoving();	
}

void AEnemy::OnCombatSphereOverlapBegin(UPrimitiveComponent* OverlappedComponent,
                                        AActor* OtherActor,
                                        UPrimitiveComponent* OtherComp,
                                        int32 OtherBodyIndex,
                                        bool bFromSweep,
                                        const FHitResult& SweepResult)
{
	if (!IsAlive() || !IsValid(OtherActor)) return;

	auto TargetCharacter = Cast<AMainCharacter>(OtherActor);

	if (!IsValid(TargetCharacter)) return;
	
	TargetCharacter->SetCombatTarget(this);
	TargetCharacter->UpdateClosestCombatTarget();
	SetCombatTarget(TargetCharacter);

	bIsCombatSphereOverlapped = true;
	AttackStart();
}

void AEnemy::OnCombatSphereOverlapEnd(UPrimitiveComponent* OverlappedComponent,
                                      AActor* OtherActor,
                                      UPrimitiveComponent* OtherComp,
                                      int32 OtherBodyIndex)
{
	if (!IsAlive() || !IsValid(OtherActor) || !IsValid(OtherComp)) return;

	auto TargetCharacter = Cast<AMainCharacter>(OtherActor);

	if (!IsValid(TargetCharacter)) return;

	bIsCombatSphereOverlapped = false;
	MoveToTarget(TargetCharacter);

	if (TargetCharacter->GetCombatTarget() == this)
	{
		TargetCharacter->SetCombatTarget(nullptr);
		TargetCharacter->UpdateClosestCombatTarget();
	}

	auto TargetCharacterController = TargetCharacter->GetPlayerController();
	
	if (IsValid(TargetCharacterController))
	{
		const auto TargetCharacterMesh = Cast<USkeletalMeshComponent>(OtherComp);

		if (IsValid(TargetCharacterMesh))
		{
			TargetCharacterController->HideEnemyHealthBar();
		}
	}
	
	GetWorldTimerManager().ClearTimer(AttackDelayTimer);
}

void AEnemy::MoveToTarget(AMainCharacter* Target)
{
	if (IsAttacking()) return;
	
	MovementStatus = EEnemyMovementStatus::EMS_MoveToPlayer;
	
	if (!IsValid(AIController)) return;

	FAIMoveRequest MoveRequest;
	// The rest of FAIMoveRequest parameters are defaults
	MoveRequest.SetGoalActor(Target);
	MoveRequest.SetAcceptanceRadius(5.0F); //distance between colliding characters' capsules

	FNavPathSharedPtr NavPath;	
	
	AIController->MoveTo(MoveRequest, &NavPath);

	/*
	// FOR DEBUG
	auto PathPoints = NavPath->GetPathPoints();
	for (auto Point : PathPoints)
	{
		auto Location = Point.Location;
		UKismetSystemLibrary::DrawDebugSphere
		(
			this,
			Location,
			25.0F,
			12,
			FLinearColor::Red,
			5.0F,
			2.0F
		);
	}
	*/
}

void AEnemy::OnCombatCollisionBoxOverlap(AActor* OverlappedActor, const FName& ParticleEmitterSocketName)
{
	if (!IsAlive() || !IsValid(OverlappedActor) || !HasCombatTarget()) return;

	auto Target = Cast<AMainCharacter>(OverlappedActor);

	if (!IsValid(Target)) return;

	ApplyDamageToTarget(Target);
	PlayTargetHitEffect(Target, ParticleEmitterSocketName);
	PlayTargetHitSound(Target);
}

void AEnemy::ApplyDamageToTarget(AActor* Target)
{
	if (!IsValid(DamageTypeClass))
	{
		ULogger::LogWarning(
			"Enemy. ApplyDamageToTarget. Can't find the desired damage type class. Please check damage type class of the weapon");
		return;
	}

	const auto ActualDamage = CalculateDamage();
	UGameplayStatics::ApplyDamage(Target, ActualDamage, AIController, this, DamageTypeClass);
}

float AEnemy::CalculateDamage() const
{
	auto ScatteredAbsValue = FMath::FRandRange(.0F, DamageScatter);
	const auto bIsPlus = FMath::RandBool();

	if (!bIsPlus) ScatteredAbsValue *= -1.0F;

	auto Result = DamageBase + ScatteredAbsValue;
	if (Result < 0) Result = 0;

	return Result;
}

void AEnemy::PlayTargetHitEffect(const AGameCharacter* Target, const FName& ParticleEmitterSocketName) const
{
	auto WeaponLocation = GetActorLocation();

	const auto SkeletalMesh = GetMesh();
	const auto WeaponDamageSocket = SkeletalMesh->GetSocketByName(ParticleEmitterSocketName);

	if (IsValid(WeaponDamageSocket))
	{
		const auto DamageSocketLocation = WeaponDamageSocket->GetSocketLocation(SkeletalMesh);
		Target->PlayHitReceiveEffect(DamageSocketLocation);
	}
}

void AEnemy::PlayTargetHitSound(const AGameCharacter* Target) const
{
	Target->PlayTakeHitSound();
}

void AEnemy::AttackStart()
{
	if (!IsAlive() || !HasCombatTarget()) return;	

	if (IsValid(AIController))
	{
		AIController->StopMovement();
		MovementStatus = EEnemyMovementStatus::EMS_Attacking;		
	}

	if (IsAttacking()) return; 	

	SetIsAttacking(true);
	SetTurnToEnemyState(true);

	PlayAttackAnimation();
}

void AEnemy::AttackEnd()
{
	SetIsAttacking(false);
	SetTurnToEnemyState(false);
	if (bIsCombatSphereOverlapped)
	{	
		AttackStartDelayed();
	}
	else
	{
		const auto CurrentCombatTarget = GetCombatTarget();

		if (IsValid(CurrentCombatTarget))
		{
			const auto TargetCharacter = Cast<AMainCharacter>(CurrentCombatTarget);			
			MoveToTarget(TargetCharacter);
		}
	}
}

void AEnemy::AttackStartDelayed()
{
	const auto AttackDelayTime = FMath::FRandRange(AttackDelayTimeMin, AttackDelayTimeMax);	
	GetWorldTimerManager().SetTimer(AttackDelayTimer, this, &AEnemy::AttackStart, AttackDelayTime);
}

void AEnemy::StopMoving()
{
	MovementStatus = EEnemyMovementStatus::EMS_Idle;
	if (!IsValid(AIController)) return;

	AIController->StopMovement();
}

void AEnemy::PlayAttackAnimation() { }
 
void AEnemy::Die(AActor* DeathCauser)
{
	MovementStatus = EEnemyMovementStatus::EMS_Dead;

	CombatSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	AggroSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SetIsAttacking(false);

	auto TargetCharacter = Cast<AMainCharacter>(DeathCauser);

	if (IsValid(TargetCharacter))
	{
		TargetCharacter->UpdateClosestCombatTarget();
	}
};

void AEnemy::DeathEnd()
{
	GetMesh()->bPauseAnims = true;
	GetMesh()->bNoSkeletonUpdate = true;

	GetWorldTimerManager().SetTimer(DeathDelayTimer, this, &AEnemy::DelayedDestroy, DeathDelayTime);
}

bool AEnemy::IsAlive() const
{
	return MovementStatus != EEnemyMovementStatus::EMS_Dead;
}

void AEnemy::DelayedDestroy()
{
	Destroy();
}

void AEnemy::PlayAttackSound() const
{
	if (!AttackSound->IsPlayable()) return;

	UGameplayStatics::PlaySound2D(this, AttackSound);
}
