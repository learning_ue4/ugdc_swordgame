// Fill out your copyright notice in the Description page of Project Settings.

#include "MinionDawn.h"
#include "Components/BoxComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "UGDC_SwordGame/Player/MainCharacter.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Kismet/GameplayStatics.h"
#include "Animation/AnimInstance.h"
#include "Sound/SoundCue.h"

AMinionDawn::AMinionDawn()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InitializeCombatCollisionBoxes();
}

void AMinionDawn::InitializeCombatCollisionBoxes()
{
	const auto SkeletalMeshComponent = GetMesh();

	LeftSwordCombatCollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("LeftSwordCollisionBox"));
	LeftSwordCombatCollisionBox->SetupAttachment(GetMesh(), FName("LeftSwordAttackSocket"));

	RightSwordCombatCollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RightSwordCollisionBox"));
	RightSwordCombatCollisionBox->SetupAttachment(GetMesh(), FName("RightSwordAttackSocket"));

	LeftTipSocketName = FName("LeftTipSocket");
	RightTipSocketName = FName("RightTipSocket");
}

// Called when the game starts or when spawned
void AMinionDawn::BeginPlay()
{
	Super::BeginPlay();

	SetupCombatCollisionBoxes();

	LeftSwordCombatCollisionBox->OnComponentBeginOverlap.AddDynamic(
		this, &AMinionDawn::OnLeftSwordCombatCollisionBoxOverlapBegin);
	LeftSwordCombatCollisionBox->OnComponentEndOverlap.AddDynamic(
		this, &AMinionDawn::OnLeftSwordCombatCollisionBoxOverlapEnd);
	RightSwordCombatCollisionBox->OnComponentBeginOverlap.AddDynamic(
		this, &AMinionDawn::OnRightSwordCombatCollisionBoxOverlapBegin);
	RightSwordCombatCollisionBox->OnComponentEndOverlap.AddDynamic(
		this, &AMinionDawn::OnRightSwordCombatCollisionBoxOverlapEnd);
}

void AMinionDawn::SetupCombatCollisionBoxes()
{
	// until ActivateCollision() is called
	LeftSwordCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	LeftSwordCombatCollisionBox->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	LeftSwordCombatCollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	LeftSwordCombatCollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn,
		ECollisionResponse::ECR_Overlap);

	RightSwordCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RightSwordCombatCollisionBox->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	RightSwordCombatCollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	RightSwordCombatCollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
}

// Called every frame
void AMinionDawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AMinionDawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AMinionDawn::OnLeftSwordCombatCollisionBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	OnCombatCollisionBoxOverlap(OtherActor, LeftTipSocketName);
}

void AMinionDawn::OnLeftSwordCombatCollisionBoxOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
}

void AMinionDawn::OnRightSwordCombatCollisionBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	OnCombatCollisionBoxOverlap(OtherActor, RightTipSocketName);
}

void AMinionDawn::OnRightSwordCombatCollisionBoxOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
}

void AMinionDawn::ActivateLeftSwordCombatCollision()
{
	LeftSwordCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AMinionDawn::DeactivateLeftSwordCombatCollision()
{
	LeftSwordCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AMinionDawn::ActivateRightSwordCombatCollision()
{
	RightSwordCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AMinionDawn::DeactivateRightSwordCombatCollision()
{
	RightSwordCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AMinionDawn::PlayAttackAnimation()
{
	auto AnimInstance = GetMesh()->GetAnimInstance();
	const auto Montage = GetCombatMontage();
	
	if (!IsValid(AnimInstance) || !IsValid(Montage)) return;

	const auto SectionNumber = FMath::RandRange(0, 2);
	switch (SectionNumber)
	{
	case 0:
		AnimInstance->Montage_Play(Montage, 1.35F);
		AnimInstance->Montage_JumpToSection(FName("Attack_01"), Montage);
		break;
	case 1:
		AnimInstance->Montage_Play(Montage, 1.35F);
		AnimInstance->Montage_JumpToSection(FName("Attack_02"), Montage);
		break;
	case 2:
		AnimInstance->Montage_Play(Montage, 1.35F);
		AnimInstance->Montage_JumpToSection(FName("Attack_03"), Montage);
		break;
	default:
		break;
	}
}

void AMinionDawn::Die(AActor* DeathCauser)
{
	LeftSwordCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RightSwordCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	auto AnimInstance = GetMesh()->GetAnimInstance();
	const auto Montage = GetCombatMontage();

	if (IsValid(AnimInstance) && IsValid(Montage))
	{
		AnimInstance->Montage_Play(Montage);
		AnimInstance->Montage_JumpToSection(FName("Death"), Montage);
	}

	Super::Die(DeathCauser);
}

void AMinionDawn::DelayedDestroy()
{
	Super::DelayedDestroy();
	Destroy();
}
