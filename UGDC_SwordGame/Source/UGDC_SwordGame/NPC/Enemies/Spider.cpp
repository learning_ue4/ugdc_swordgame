// Fill out your copyright notice in the Description page of Project Settings.

#include "Spider.h"
#include "Components/BoxComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "UGDC_SwordGame/Player/MainCharacter.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Kismet/GameplayStatics.h"
#include "Animation/AnimInstance.h"
#include "AIController.h"
#include "Sound/SoundCue.h"

// Sets default values
ASpider::ASpider()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InitializeCombatCollisionBoxes();
}

void ASpider::InitializeCombatCollisionBoxes()
{
	const auto SkeletalMeshComponent = GetMesh();
	
	LeftClawCombatCollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("LeftClawCollisionBox"));
	LeftClawCombatCollisionBox->SetupAttachment(GetMesh(), FName("LeftClawAttackSocket"));
	
	RightClawCombatCollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("RightClawCollisionBox"));	
	RightClawCombatCollisionBox->SetupAttachment(GetMesh(), FName("RightClawAttackSocket"));

	LeftTipSocketName = FName("LeftTipSocket");
	RightTipSocketName = FName("RightTipSocket");
}

// Called when the game starts or when spawned
void ASpider::BeginPlay()
{
	Super::BeginPlay();

	SetupCombatCollisionBoxes();

	LeftClawCombatCollisionBox->OnComponentBeginOverlap.AddDynamic(
		this, &ASpider::OnLeftClawCombatCollisionBoxOverlapBegin);
	LeftClawCombatCollisionBox->OnComponentEndOverlap.AddDynamic(
		this, &ASpider::OnLeftClawCombatCollisionBoxOverlapEnd);
	RightClawCombatCollisionBox->OnComponentBeginOverlap.AddDynamic(
		this, &ASpider::OnRightClawCombatCollisionBoxOverlapBegin);
	RightClawCombatCollisionBox->OnComponentEndOverlap.AddDynamic(
		this, &ASpider::OnRightClawCombatCollisionBoxOverlapEnd);
}

void ASpider::SetupCombatCollisionBoxes()
{
	// until ActivateCollision() is called
	LeftClawCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);	
	LeftClawCombatCollisionBox->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	LeftClawCombatCollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	LeftClawCombatCollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn,
	                                                          ECollisionResponse::ECR_Overlap);

	RightClawCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);	
	RightClawCombatCollisionBox->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	RightClawCombatCollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	RightClawCombatCollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn,
	                                                           ECollisionResponse::ECR_Overlap);
}

// Called every frame
void ASpider::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ASpider::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void ASpider::OnLeftClawCombatCollisionBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent,
                                                       AActor* OtherActor,
                                                       UPrimitiveComponent* OtherComp,
                                                       int32 OtherBodyIndex,
                                                       bool bFromSweep,
                                                       const FHitResult& SweepResult)
{
	OnCombatCollisionBoxOverlap(OtherActor, LeftTipSocketName);
}

void ASpider::OnLeftClawCombatCollisionBoxOverlapEnd(UPrimitiveComponent* OverlappedComponent,
                                                     AActor* OtherActor,
                                                     UPrimitiveComponent* OtherComp,
                                                     int32 OtherBodyIndex)
{
}

void ASpider::OnRightClawCombatCollisionBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent,
                                                        AActor* OtherActor,
                                                        UPrimitiveComponent* OtherComp,
                                                        int32 OtherBodyIndex,
                                                        bool bFromSweep,
                                                        const FHitResult& SweepResult)
{
	OnCombatCollisionBoxOverlap(OtherActor, RightTipSocketName);
}

void ASpider::OnRightClawCombatCollisionBoxOverlapEnd(UPrimitiveComponent* OverlappedComponent,
                                                      AActor* OtherActor,
                                                      UPrimitiveComponent* OtherComp,
                                                      int32 OtherBodyIndex)
{
}

void ASpider::ActivateLeftClawCombatCollision()
{
	LeftClawCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void ASpider::DeactivateLeftClawCombatCollision()
{
	LeftClawCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ASpider::ActivateRightClawCombatCollision()
{
	RightClawCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void ASpider::DeactivateRightClawCombatCollision()
{
	RightClawCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ASpider::PlayAttackAnimation()
{
	auto AnimInstance = GetMesh()->GetAnimInstance();
	const auto Montage = GetCombatMontage();

	if (!IsValid(AnimInstance) || !IsValid(Montage)) return;

	const auto SectionNumber = FMath::RandRange(0, 1);	
	switch (SectionNumber)
	{
	case 0:
		AnimInstance->Montage_Play(Montage, 1.35F);
		AnimInstance->Montage_JumpToSection(FName("Attack_01"), Montage);
		break;
	case 1:
		AnimInstance->Montage_Play(Montage, 1.35F);
		AnimInstance->Montage_JumpToSection(FName("Attack_02"), Montage);
		break;
	default:
		break;
	}
}

void ASpider::Die(AActor* DeathCauser)
{
	LeftClawCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RightClawCombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	
	auto AnimInstance = GetMesh()->GetAnimInstance();
	const auto Montage = GetCombatMontage();
	
	if (IsValid(AnimInstance) && IsValid(Montage))
	{
		AnimInstance->Montage_Play(Montage);
		AnimInstance->Montage_JumpToSection(FName("Death"), Montage);
	}

	Super::Die(DeathCauser);
}

void ASpider::DelayedDestroy()
{
	Super::DelayedDestroy();
	Destroy();
}