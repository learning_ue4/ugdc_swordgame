// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "UGDC_SwordGame/NPC/Enemy.h"
#include "Spider.generated.h"

/*
 * Spider enemy and all its subspecies
 */
UCLASS()
class UGDC_SWORDGAME_API ASpider : public AEnemy
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ASpider();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	/* Combat collision of spider's left claw */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* LeftClawCombatCollisionBox;

	/* Combat collision of spider's right claw */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	UBoxComponent* RightClawCombatCollisionBox;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	FName LeftTipSocketName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	FName RightTipSocketName;
	
	/* Initialize claws' collision boxes */
	void InitializeCombatCollisionBoxes();

	/* Set fangs' collision boxes default parameters */
	void SetupCombatCollisionBoxes();

	/* Events occured on start colliding (overlapping) with left claw collision box */
	UFUNCTION()
	void OnLeftClawCombatCollisionBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                                              AActor* OtherActor,
	                                              UPrimitiveComponent* OtherComp,
	                                              int32 OtherBodyIndex,
	                                              bool bFromSweep,
	                                              const FHitResult& SweepResult);

	/* Events occured on end colliding (overlapping) with left claw collision box */
	UFUNCTION()
	void OnLeftClawCombatCollisionBoxOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	                                            AActor* OtherActor,
	                                            UPrimitiveComponent* OtherComp,
	                                            int32 OtherBodyIndex);

	/* Events occured on start colliding (overlapping) with right claw collision box */
	UFUNCTION()
	void OnRightClawCombatCollisionBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                                               AActor* OtherActor,
	                                               UPrimitiveComponent* OtherComp,
	                                               int32 OtherBodyIndex,
	                                               bool bFromSweep,
	                                               const FHitResult& SweepResult);

	/* Events occured on end colliding (overlapping) with right claw collision box (hit with a weapon) */
	UFUNCTION()
	void OnRightClawCombatCollisionBoxOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	                                             AActor* OtherActor,
	                                             UPrimitiveComponent* OtherComp,
	                                             int32 OtherBodyIndex);


	/* Activate left claw combat collision - called from blueprint */
	UFUNCTION(BlueprintCallable)
	void ActivateLeftClawCombatCollision();

	/* Deactivate left claw combat collision - called from blueprint */
	UFUNCTION(BlueprintCallable)
	void DeactivateLeftClawCombatCollision();

	/* Activate right claw combat collision - called from blueprint */
	UFUNCTION(BlueprintCallable)
	void ActivateRightClawCombatCollision();

	/* Deactivate right claw combat collision - called from blueprint */
	UFUNCTION(BlueprintCallable)
	void DeactivateRightClawCombatCollision();

	/* Play attack animation of this current enemy, overriden */
	void PlayAttackAnimation() override;

	/* Release all enemy's resources - should be called within SetTimer() and use DeathDelayTime */
	void DelayedDestroy() override;

	/* Pass away */
	void Die(AActor* DeathCauser) override;
};
