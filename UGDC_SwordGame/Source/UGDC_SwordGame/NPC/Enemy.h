// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "UGDC_SwordGame/Common/GameCharacter.h"
#include "UGDC_SwordGame/Player/MainCharacter.h"
#include "Enemy.generated.h"

/*
 * Enum with statuses related to enemy's movement
 */
UENUM(BlueprintType)
enum class EEnemyMovementStatus : uint8
{
	EMS_Idle			UMETA(DisplayName = "Idle"),
	EMS_MoveToPlayer	UMETA(DisplayName = "MoveToPlayer"),
	EMS_Attacking		UMETA(DisplayName = "Attacking"),
	EMS_Dead			UMETA(DisplayName = "Dead"),
	EMX_MAX				UMETA(DisplayName = "DefaultMAX")
};

UCLASS()
class UGDC_SWORDGAME_API AEnemy : public AGameCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* Actions performed when target is overlapped by enemy's combat collision box*/
	void OnCombatCollisionBoxOverlap(AActor* OverlappedActor, const FName& ParticleEmitterSocketName);

	/* Release all enemy's resources - should be called within SetTimer() and use DeathDelayTime */
	virtual void DelayedDestroy();

	/* Play attack animation from certain enemy's combat montage - needs to be overriden */
	virtual void PlayAttackAnimation();

	/* Pass away */
	virtual void Die(AActor* DeathCauser) override;

	FORCEINLINE class AAIController* GetAIController() const { return AIController; }
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;	

	/* Sets the AI Controller - used when spawning from external places (spawn volumes, conjuring, etc.) */
	FORCEINLINE void SetAIController(class AAIController* AIControllerToSet) { AIController = AIControllerToSet; }

private:
	/* AI Controller cast to controller from Pawn component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	AAIController* AIController;
	
	/* Movement status that the creature is currently in */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	EEnemyMovementStatus MovementStatus;

	/* Enemy becomes aggressive when player overlaps this sphere */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	class USphereComponent* AggroSphere;

	/* Radius of aggro sphere */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	float AggroSphereRadius;

	/* Enemy attacks player when he overlaps this sphere */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	USphereComponent* CombatSphere;

	/* Radius of combat sphere */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	float CombatSphereRadius;

	/* Is target currently overlapping enemy's combat sphere */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = AI, meta = (AllowPrivateAccess = "true"))
	bool bIsCombatSphereOverlapped;

	/* Base damage dealt by enemy for damage calculations */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Stats, meta = (AllowPrivateAccess = "true"))
	float DamageBase;

	/* Scatter damage dealt by enemy for damage calculations */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Stats, meta = (AllowPrivateAccess = "true"))
	float DamageScatter;

	/* A timer that handles delay between attacks */
	FTimerHandle AttackDelayTimer;

	/* A timer that handles delay between death of an enemy and releasing its resources */
	FTimerHandle DeathDelayTimer;

	/* Seconds between enemy's death and releasing its resources */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	float DeathDelayTime;

	/* Minimum attack delay time, seconds */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	float AttackDelayTimeMin;

	/* Maximum attack delay time, seconds */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	float AttackDelayTimeMax;

	/* Damage class of enemy's built-in weapon */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UDamageType> DamageTypeClass;

	/* Sound played when attacking */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	class USoundCue* AttackSound;

	/* Initialize default parameters */
	void InitializeDefaultValues();

	/* Initialize AIController and related stuff */
	void InitializeAI();

	/* Stops enemy's movement and returning to initial state */
	void StopMoving();

	/* Set camera not to focus on enemy when close to combat */
	void SetIgnoreCamera();

	/* Actual damage dealt by enemy */
	float CalculateDamage() const;

	/* Apply calculated damage to target */
	void ApplyDamageToTarget(AActor* Target);

	/* Events occured on start colliding (overlapping) with aggro sphere */
	UFUNCTION()
	void OnAggroSphereOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                               AActor* OtherActor,
	                               UPrimitiveComponent* OtherComp,
	                               int32 OtherBodyIndex,
	                               bool bFromSweep,
	                               const FHitResult& SweepResult);

	/* Events occured on end colliding (overlapping) with aggro sphere */
	UFUNCTION()
	void OnAggroSphereOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	                             AActor* OtherActor,
	                             UPrimitiveComponent* OtherComp,
	                             int32 OtherBodyIndex);


	/* Events occured on start colliding (overlapping) with combat sphere */
	UFUNCTION()
	void OnCombatSphereOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                                AActor* OtherActor,
	                                UPrimitiveComponent* OtherComp,
	                                int32 OtherBodyIndex,
	                                bool bFromSweep,
	                                const FHitResult& SweepResult);

	/* Events occured on end colliding (overlapping) with combat sphere */
	UFUNCTION()
	void OnCombatSphereOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	                              AActor* OtherActor,
	                              UPrimitiveComponent* OtherComp,
	                              int32 OtherBodyIndex);

	/* Go into attacking state and perform an attack */
	virtual void AttackStart() override;

	/* Go into attack state and perform a delayed attack */
	void AttackStartDelayed();

	/* End attack and go out of attacking state */
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	void AttackEnd();

	/* What happens after death animation end */
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	void DeathEnd();

	/* Move to the player */
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	void MoveToTarget(class AMainCharacter* Target);

	/* Check if is alive */
	bool IsAlive() const;

	/* Play SFX when target is being hit */
	void PlayTargetHitSound(const AGameCharacter* Enemy) const;

	/* Play particle effect when target is being hit */
	void PlayTargetHitEffect(const AGameCharacter* Target, const FName& ParticleEmitterSocketName) const;

	/* Play sound when attacking */
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	void PlayAttackSound() const;
};
