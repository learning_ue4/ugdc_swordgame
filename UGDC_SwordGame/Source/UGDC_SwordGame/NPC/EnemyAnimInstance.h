// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "UGDC_SwordGame/Common/CommonAnimInstance.h"
#include "Enemy.h"
#include "EnemyAnimInstance.generated.h"

/*
 * Animation instance of an enemy 
 */
UCLASS()
class UGDC_SWORDGAME_API UEnemyAnimInstance : public UCommonAnimInstance
{
	GENERATED_BODY()

private:
	/* The enemy itself */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class AEnemy* Enemy;

	void NativeInitializeAnimation() override;

	/* An analogue of Tick() function */
	void UpdateAnimationProperties() override;
	
};
