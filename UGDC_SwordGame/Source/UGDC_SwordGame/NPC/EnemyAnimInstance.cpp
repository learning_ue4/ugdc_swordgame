// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAnimInstance.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Enemy.h"

void UEnemyAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	const auto EnemyPawn = GetCharacterPawn();
	
	if (IsValid(EnemyPawn))
	{
		Enemy = Cast<AEnemy>(EnemyPawn);
	}
	else
	{
		Enemy = nullptr;
	}
}

void UEnemyAnimInstance::UpdateAnimationProperties()
{
	Super::UpdateAnimationProperties();

	const auto EnemyPawn = GetCharacterPawn();

	if (IsValid(EnemyPawn))
	{		
		const auto CharacterSpeed = EnemyPawn->GetVelocity();

		// When on ground - don't care about Z speed (Z speed is for jumpin' and fallin' and has separate logic
		const auto LateralSpeed = FVector(CharacterSpeed.X, CharacterSpeed.Y, 0.F);		
		SetMovementSpeed(LateralSpeed.Size());

		const auto bIsFalling = EnemyPawn->GetMovementComponent()->IsFalling();
		SetInAir(bIsFalling);
	}
}