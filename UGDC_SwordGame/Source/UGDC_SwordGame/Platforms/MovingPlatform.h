// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MovingPlatform.generated.h"

/*
 * Platform, moving from one point in space to another 
 */
UCLASS()
class UGDC_SWORDGAME_API AMovingPlatform final : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMovingPlatform();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
private:
	/* A mesh of an actual platform */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = MovingPlatform, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComponent;

	/* Can platform move */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = MovingPlatform, meta = (AllowPrivateAccess = "true"))
	bool bIsFixed;

	/* A point, from which platform starts moving */
	UPROPERTY(EditAnywhere)
	FVector StartPoint;

	/* A point, where platform ends moving */
	UPROPERTY(EditAnywhere, meta = (MakeEditWidget = "true"), meta = (AllowPrivateAccess = "true"))
	FVector EndPoint;

	/* Speed with which platform moves from start point to end point */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = MovingPlatform, meta = (AllowPrivateAccess = "true"))
	float MovingSpeed;

	/* Time for platform to stay in start and end position between moving */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = MovingPlatform, meta = (AllowPrivateAccess = "true"))
	float InterpolationTime;
	
	/* Handler for time between moving back and forth */
	FTimerHandle InterpolationTimer;

	/* Is platform moving */
	bool bIsMoving;

	/* Distance between start and end points */
	float Distance;

	/* A precision to check if distance is equivalent to zero */
	float DistancePrecision;

	/* Initialize object parameters with default values */
	void InitDefaults();

	/* Swap vectors of movement to switch movement from 'forward' to 'backwards' */
	void SwapVectors(FVector& VectorA, FVector& VectorB);

	/* Revert interpolating state */
	void ToggleInterpolating();
};
