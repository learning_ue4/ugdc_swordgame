// Fill out your copyright notice in the Description page of Project Settings.


#include "MovingPlatform.h"
#include "Components/StaticMeshComponent.h"
#include "TimerManager.h"

// Sets default values
AMovingPlatform::AMovingPlatform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;	
	
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MovingPlatformMesh"));
	RootComponent = MeshComponent;
	InitDefaults();
}

void AMovingPlatform::InitDefaults()
{
	DistancePrecision = 0.1F;

	StartPoint = FVector(.0F);
	EndPoint = FVector(.0F);

	bIsFixed = false;
	bIsMoving = false;
}

// Called when the game starts or when spawned
void AMovingPlatform::BeginPlay()
{
	Super::BeginPlay();

	StartPoint = GetActorLocation();
	// Correcting world coords of the end point
	EndPoint += StartPoint;

	GetWorldTimerManager().SetTimer(InterpolationTimer, this, &AMovingPlatform::ToggleInterpolating, InterpolationTime);

	Distance = (EndPoint - StartPoint).Size();
}

// Called every frame
void AMovingPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsFixed) return;
	
	if (!bIsMoving) return;	

	const auto CurrentLocation = GetActorLocation();
	const auto InterpolatedLocation = FMath::VInterpTo(CurrentLocation, EndPoint, DeltaTime, MovingSpeed);
	SetActorLocation(InterpolatedLocation);

	const auto DistanceTravelled = (GetActorLocation() - StartPoint).Size();
	if ((Distance - DistanceTravelled) <= DistancePrecision)
	{
		ToggleInterpolating();
		GetWorldTimerManager().SetTimer(InterpolationTimer, this, &AMovingPlatform::ToggleInterpolating, InterpolationTime);
		SwapVectors(StartPoint, EndPoint);
	}
}

void AMovingPlatform::ToggleInterpolating()
{
	if (bIsFixed) return;
	
	bIsMoving = !bIsMoving;
}

void AMovingPlatform::SwapVectors(FVector& VectorA, FVector& VectorB)
{
	const auto Temp = VectorA;
	VectorA = VectorB;
	VectorB = Temp;
}