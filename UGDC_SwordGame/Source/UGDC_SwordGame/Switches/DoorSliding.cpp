// Fill out your copyright notice in the Description page of Project Settings.


#include "DoorSliding.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ADoorSliding::ADoorSliding()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;	
}

// Called when the game starts or when spawned
void ADoorSliding::BeginPlay()
{
	Super::BeginPlay();	
}

// Called every frame
void ADoorSliding::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ADoorSliding::UpdateDoorElevation(float ZValue)
{
	auto Location = GetInitialMeshLocation();
	if (bIsDownwards)
	{
		Location.Z -= ZValue;		
	}
	else
	{
		Location.Z += ZValue;
	}

	const auto DoorMeshComponent = GetMeshComponent();
	if (IsValid(DoorMeshComponent))
	{
		DoorMeshComponent->SetWorldLocation(Location);
	}
}
