// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableSwitchReceiver.h"
#include "DoorSliding.generated.h"

/*
 * A door, sliding vertically
 */
UCLASS()
class UGDC_SWORDGAME_API ADoorSliding final : public AInteractableSwitchReceiver
{
	GENERATED_BODY()
	
public:		
	ADoorSliding();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	/* Is Door sliding downwards? By default is false i.e. slides upwards */
	UPROPERTY(EditInstanceOnly, Category = DoorSliding, meta = (AllowPrivateAccess = "true"))
	bool bIsDownwards;

	/* Update door's Z position */
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	void UpdateDoorElevation(float ZValue);
};
