// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableSwitchReceiver.h"
#include "ConfigurableSwitch.generated.h"

/*
 * A configurable switch in general with array of
 * objects (receivers) it affects
 */
UCLASS(Abstract)
class UGDC_SWORDGAME_API AConfigurableSwitch : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AConfigurableSwitch();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* The mesh component of a switch */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = Switch)
	class UStaticMeshComponent* MeshComponent;

	/* Send switch ON signal to all receivers and perform their actions */
	void SwitchReceiversOn();

	/* Send switch OFF signal to all receivers and perform their actions */
	void SwitchReceiversOff();

	/* *****FOR DEBUG***** */
	/* Draw arrows in editor from the switch to all receivers */
	UFUNCTION(CallInEditor, Category = SwitchConnections)
	void DrawDebugConnections();

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	/* Timer handler for SwitchTime */
	FTimerHandle TimerHandle;

	/* Time it needs from switching ON to make action by receiver */
	UPROPERTY(EditInstanceOnly)
	float SwitchOnTime;

	/* Time it needs from switching OFF to make action by receiver */
	UPROPERTY(EditInstanceOnly)
	float SwitchOffTime;

	/* Actors, affected by the switch */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = Switch, meta = (AllowPrivateAccess = true))
	TArray<class AInteractableSwitchReceiver*> Receivers;

	/* Init default values */
	void InitDefaults();
};
