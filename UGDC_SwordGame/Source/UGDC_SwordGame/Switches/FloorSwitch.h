// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ConfigurableSwitch.h"
#include "FloorSwitch.generated.h"

/*
 * Floor switch, that's activated by standing on it
 */
UCLASS()
class UGDC_SWORDGAME_API AFloorSwitch final : public AConfigurableSwitch
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AFloorSwitch();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/* Physically raise the mesh */
	UFUNCTION(BlueprintImplementableEvent, Category = FloorSwitch)
	void RaiseFloorSwitch();

	/* Physically lower the mesh */
	UFUNCTION(BlueprintImplementableEvent, Category = FloorSwitch)
	void LowerFloorSwitch();

private:
	/* A box-shaped overlap volume that trigger smth if character enters it */
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = FloorSwitch, meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* TriggerBox;

	/* Initial location for the floor switch */
	UPROPERTY(BlueprintReadOnly, Category = FloorSwitch, meta = (AllowPrivateAccess = "true"))
	FVector InitialMeshLocation;

	/* Is character currently standing on a switch */
	bool bIsStandingOnSwitch;

	/* Default size of a trigger box */
	FVector DefaultOverlapBoxExtent;

	/* Events occured on start standing on the switch */
	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                    AActor* OtherActor,
	                    UPrimitiveComponent* OtherComp,
	                    int32 OtherBodyIndex,
	                    bool bFromSweep,
	                    const FHitResult& SweepResult);

	/* Events occured on going off the switch */
	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	                  AActor* OtherActor,
	                  UPrimitiveComponent* OtherComp,
	                  int32 OtherBodyIndex);

	/* Update switch's Z position */
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	void UpdateFloorSwitchElevation(float ZValue);
	
	/* Init default values of parameters */
	void InitFloorSwitchDefaults();

	/* Init trigger box and root component (they're connected) */
	void InitTriggerBoxAndRootComponent();

	/* Init mesh component of the switch */
	void InitMeshComponent();
};
