// Fill out your copyright notice in the Description page of Project Settings.


#include "ConfigurableSwitch.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Kismet/KismetSystemLibrary.h"

// Sets default values
AConfigurableSwitch::AConfigurableSwitch()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InitDefaults();
}

void AConfigurableSwitch::InitDefaults()
{		
	SwitchOnTime = 0.3;		
	SwitchOffTime = 0.3;
}

// Called when the game starts or when spawned
void AConfigurableSwitch::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AConfigurableSwitch::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AConfigurableSwitch::SwitchReceiversOn()
{
	for (auto& Receiver : Receivers)
	{
		if (!IsValid(Receiver)) continue;
		
		GetWorldTimerManager().SetTimer(TimerHandle, Receiver, &AInteractableSwitchReceiver::OnSwitchOn, SwitchOffTime);
	}
}

void AConfigurableSwitch::SwitchReceiversOff()
{
	for (auto& Receiver : Receivers)
	{
		if (!IsValid(Receiver)) continue;
		
		GetWorldTimerManager().SetTimer(TimerHandle, Receiver, &AInteractableSwitchReceiver::OnSwitchOff,
		                                SwitchOffTime);
	}
}

void AConfigurableSwitch::DrawDebugConnections()
{
	auto World = GetWorld();
	const auto Location = GetActorLocation();
	const auto ViewOffset = FVector(.0F, .0F, 25.0F);

	for (const auto& Receiver : Receivers)
	{
		if (Receiver == nullptr) continue;

		auto ReceiverLocation = Receiver->GetActorLocation();
		
		UKismetSystemLibrary::DrawDebugArrow
		(
			this,
			Location + ViewOffset,
			ReceiverLocation + ViewOffset,
			(ReceiverLocation - Location).Size(),
			FLinearColor::Green,
			60.0F,
			3.0F
		);
	}
}
