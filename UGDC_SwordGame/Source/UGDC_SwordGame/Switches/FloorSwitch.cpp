// Fill out your copyright notice in the Description page of Project Settings.

#include "FloorSwitch.h"
#include "Components/BoxComponent.h"
#include "Components/StaticMeshComponent.h"
#include "InteractableSwitchReceiver.h"

// Sets default values
AFloorSwitch::AFloorSwitch()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InitFloorSwitchDefaults();
	InitTriggerBoxAndRootComponent();
	InitMeshComponent();
}

void AFloorSwitch::InitFloorSwitchDefaults()
{
	bIsStandingOnSwitch = false;
	DefaultOverlapBoxExtent = FVector(62.f, 62.f, 32.f);
}

void AFloorSwitch::InitTriggerBoxAndRootComponent()
{
	TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerBox"));
	RootComponent = TriggerBox;

	TriggerBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	TriggerBox->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
	TriggerBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	TriggerBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

	TriggerBox->SetBoxExtent(DefaultOverlapBoxExtent);
}

void AFloorSwitch::InitMeshComponent()
{
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FloorSwitch"));
	MeshComponent->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AFloorSwitch::BeginPlay()
{
	Super::BeginPlay();

	TriggerBox->OnComponentBeginOverlap.AddDynamic(this, &AFloorSwitch::OnOverlapBegin);
	TriggerBox->OnComponentEndOverlap.AddDynamic(this, &AFloorSwitch::OnOverlapEnd);

	InitialMeshLocation = MeshComponent->GetComponentLocation();
}

// Called every frame
void AFloorSwitch::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);		
}

void AFloorSwitch::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (!bIsStandingOnSwitch) bIsStandingOnSwitch = true;

	LowerFloorSwitch();
	SwitchReceiversOn();
}

void AFloorSwitch::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
	if (bIsStandingOnSwitch) bIsStandingOnSwitch = false;

	RaiseFloorSwitch();
	SwitchReceiversOff();
}

void AFloorSwitch::UpdateFloorSwitchElevation(float ZValue)
{
	auto Location = InitialMeshLocation;
	Location.Z += ZValue;
	MeshComponent->SetWorldLocation(Location);
}
