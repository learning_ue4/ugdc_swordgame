// Fill out your copyright notice in the Description page of Project Settings.

#include "InteractableSwitchReceiver.h"
#include "Components/StaticMeshComponent.h"
#include "Logger.h"

// Sets default values
AInteractableSwitchReceiver::AInteractableSwitchReceiver()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = nullptr;

	InitMeshAndRootComponents();
}

void AInteractableSwitchReceiver::InitMeshAndRootComponents()
{
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("InteractableSwitchReceiverMeshComponent"));
	RootComponent = MeshComponent;
}

// Called when the game starts or when spawned
void AInteractableSwitchReceiver::BeginPlay()
{
	Super::BeginPlay();

	if (MeshComponent->IsValidLowLevel())
	{
		InitialMeshLocation = MeshComponent->GetComponentLocation();
	}
	else
	{
		ULogger::LogWarning("InteractableSwichReceiver. BeginPlay. Failed to load MeshComponent!!");
	}	
}

// Called every frame
void AInteractableSwitchReceiver::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
