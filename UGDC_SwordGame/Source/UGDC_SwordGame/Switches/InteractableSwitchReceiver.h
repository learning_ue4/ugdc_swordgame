// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "InteractableSwitchReceiver.generated.h"

/*
 * Object in game scene that can be affected by a configurable switch
 */
UCLASS(Abstract)
class UGDC_SWORDGAME_API AInteractableSwitchReceiver : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractableSwitchReceiver();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FORCEINLINE class UStaticMeshComponent* GetMeshComponent() const { return MeshComponent; }
	
	FORCEINLINE FVector GetInitialMeshLocation() const { return InitialMeshLocation; }	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/* Function to be declared in blueprint that fires when the corresponding switch is ON */
	UFUNCTION(BlueprintImplementableEvent, Category = SwitchReceiver)
	void OnSwitchOn();

	/* Function to be declared in blueprint that fires when the corresponding switch is OFF */
	UFUNCTION(BlueprintImplementableEvent, Category = SwitchReceiver)
	void OnSwitchOff();

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = SwitchReceiver, meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = SwitchReceiver, meta = (AllowPrivateAccess = "true"))
	FVector InitialMeshLocation;
	
	void InitMeshAndRootComponents();
};
