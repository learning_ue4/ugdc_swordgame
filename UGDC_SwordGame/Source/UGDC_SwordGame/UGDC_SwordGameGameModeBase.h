// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UGDC_SwordGameGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UGDC_SWORDGAME_API AUGDC_SwordGameGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
