// Fill out your copyright notice in the Description page of Project Settings.


#include "Consumable.h"
#include "UGDC_SwordGame/Player/MainCharacter.h"

// Sets default values
AConsumable::AConsumable()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bCanBeConsumed = true;
}

// Called when the game starts or when spawned
void AConsumable::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AConsumable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AConsumable::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
                                 AActor* OtherActor,
                                 UPrimitiveComponent* OtherComp,
                                 int32 OtherBodyIndex,
                                 bool bFromSweep,
                                 const FHitResult& SweepResult)
{
	auto MainCharacter = Cast<AMainCharacter>(OtherActor);

	if (!IsValid(MainCharacter)) return;

	OnPickupBP(MainCharacter);

	if (!bCanBeConsumed) return;

	Super::OnOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	MainCharacter->PickupLocations.Add(GetActorLocation());
	Destroy();
}

void AConsumable::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
                               AActor* OtherActor,
                               UPrimitiveComponent* OtherComp,
                               int32 OtherBodyIndex)
{
	Super::OnOverlapEnd(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);
}
