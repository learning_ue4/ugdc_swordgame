// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"
#include "Components/SkeletalMeshComponent.h"
#include "UGDC_SwordGame/Player/MainCharacter.h"
#include "UGDC_SwordGame/NPC/Enemy.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/BoxComponent.h"
#include "Engine/World.h"
#include "LoggingModule/Public/Logger.h"


AWeapon::AWeapon()
{
	InitDefaults();
	InitializeSkeletalMesh();
	InitializeCombatCollisionBox();
}

void AWeapon::InitializeSkeletalMesh()
{
	SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMeshComponent->SetupAttachment(RootComponent);
}

void AWeapon::InitDefaults()
{	
	State = EWeaponState::EWS_Pickup;
	DamageBase = 25.0F;
	DamageScatter = 5.0F;
	
	OnEquipSound = nullptr;
	AttackSound = nullptr;
	WeaponInstigator = nullptr;
}

void AWeapon::InitializeCombatCollisionBox()
{
	CombatCollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("WeaponCollisionBox"));
	CombatCollisionBox->SetupAttachment(RootComponent);
}

void AWeapon::InitializeIdleParticleSystemComponent()
{
	Super::InitializeIdleParticleSystemComponent();
	
	EquippedParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("EquippedParticleSystemComponent"));
	EquippedParticleSystemComponent->SetupAttachment(RootComponent);
}

void AWeapon::BeginPlay()
{
	Super::BeginPlay();

	CombatCollisionBox->OnComponentBeginOverlap.AddDynamic(this, &AWeapon::OnCombatCollisionBoxOverlapBegin);
	CombatCollisionBox->OnComponentEndOverlap.AddDynamic(this, &AWeapon::OnCombatCollisionBoxOverlapEnd);

	// doesn't work in constructor, have to call it from here
	SetupCollisionBox();
}

void AWeapon::SetupCollisionBox()
{
	CombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision); // until AcivateCollision() is called
	CombatCollisionBox->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	CombatCollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	CombatCollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
}

void AWeapon::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
                             AActor* OtherActor,
                             UPrimitiveComponent* OtherComp,
                             int32 OtherBodyIndex,
                             bool bFromSweep,
                             const FHitResult& SweepResult)
{
	Super::OnOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

	if (State == EWeaponState::EWS_Equipped) return;

	if (!IsValid(OtherActor)) return;

	auto MainCharacter = Cast<AMainCharacter>(OtherActor);

	if (!IsValid(MainCharacter)) return;

	MainCharacter->SetActiveOverlappingItem(this);
}

void AWeapon::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
                           AActor* OtherActor,
                           UPrimitiveComponent* OtherComp,
                           int32 OtherBodyIndex)
{
	Super::OnOverlapEnd(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);

	if (!IsValid(OtherActor)) return;

	auto MainCharacter = Cast<AMainCharacter>(OtherActor);

	if (!IsValid(MainCharacter)) return;

	MainCharacter->SetActiveOverlappingItem(nullptr);
}

void AWeapon::OnInteract(AMainCharacter* Interacting)
{
	Super::OnInteract(Interacting);
	Equip(Interacting);
}

void AWeapon::Equip(AMainCharacter* Character)
{
	if (!IsValid(Character)) return;

	WeaponInstigator = Character->GetController();

	// To make sure that the camera doesn't zoom in on the weapon if it's between player and weapon
	SkeletalMeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	// No collision between player's pawn and the weapon
	SkeletalMeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	// Weapon doesn't have it's own physics	since it's equipped
	SkeletalMeshComponent->SetSimulatePhysics(false);

	const USkeletalMeshSocket* RightHandSocket = Character->GetMesh()->GetSocketByName("RightHandSocket");

	if (!IsValid(RightHandSocket))
	{
		ULogger::LogWarning("Weapon. Equip. Can't find the desired equip socket. Please check socket names of a skeletal mesh");
		return;
	}
	
	StopRotation();
	RightHandSocket->AttachActor(this, Character->GetMesh());

	Character->SetEquippedWeapon(this);
	SetWeaponState(EWeaponState::EWS_Equipped);
	Character->SetActiveOverlappingItem(nullptr);

	DeactivateIdleParticleSystem();

	if (IsValid(EquippedParticleSystemComponent))
	{
		EquippedParticleSystemComponent->Activate();
	}

	PlayOnEquipSound();
}

void AWeapon::OnCombatCollisionBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent,
                                               AActor* OtherActor,
                                               UPrimitiveComponent* OtherComp,
                                               int32 OtherBodyIndex,
                                               bool bFromSweep,
                                               const FHitResult& SweepResult)
{
	if (!IsValid(OtherActor)) return;

	const auto Enemy = Cast<AEnemy>(OtherActor);
	if (!IsValid(Enemy))
	{
		ULogger::LogWarning("Weapon. OnCombatCollisionBoxOverlapBegin. Can't properly cast an actor to an enemy!");
		return;
	}

	ApplyDamageToTarget(Enemy);
	PlayEnemyHitEffect(Enemy);	
	PlayEnemyHitSound(Enemy);
}

void AWeapon::OnCombatCollisionBoxOverlapEnd(UPrimitiveComponent* OverlappedComponent,
                                             AActor* OtherActor,
                                             UPrimitiveComponent* OtherComp,
                                             int32 OtherBodyIndex)
{
}


void AWeapon::ActivateCollision()
{
	CombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AWeapon::DeactivateCollision()
{
	CombatCollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AWeapon::PlayOnEquipSound() const
{
	if (OnEquipSound == nullptr) return;

	UGameplayStatics::PlaySound2D(this, OnEquipSound);
}

void AWeapon::ApplyDamageToTarget(AActor* Target)
{

	if (!IsValid(DamageTypeClass))
	{
		ULogger::LogWarning(
			"Weapon. ApplyDamage. Can't find the desired damage type class. Please check damage type class of the weapon");
		return;
	}

	const auto ActualDamage = CalculateDamage();
	UGameplayStatics::ApplyDamage(Target, ActualDamage, WeaponInstigator, this, DamageTypeClass);
}

float AWeapon::CalculateDamage() const
{
	auto ScatteredAbsValue = FMath::FRandRange(.0F, DamageScatter);
	const auto bIsPlus = FMath::RandBool();

	if (!bIsPlus) ScatteredAbsValue *= -1.0F;
	
	auto Result = DamageBase + ScatteredAbsValue;
	if (Result < 0) Result = 0;

	return Result;
}

void AWeapon::PlayEnemyHitEffect(const AEnemy* Enemy) const
{
	auto WeaponLocation = GetActorLocation();
	const auto WeaponDamageSocket = SkeletalMeshComponent->GetSocketByName(FName("DamageSocket"));
	if (!IsValid(WeaponDamageSocket))
	{
		ULogger::LogWarning(
			"Weapon. PlayEnemyHitEffect. Can't find weapon damage socket, particle effect can not be played.");
		return;
	}

	const auto DamageSocketLocation = WeaponDamageSocket->GetSocketLocation(SkeletalMeshComponent);
	Enemy->PlayHitReceiveEffect(DamageSocketLocation);
}

void AWeapon::PlayEnemyHitSound(const AEnemy* Enemy) const
{
	Enemy->PlayTakeHitSound();
}

void AWeapon::PlayAttackSound() const
{
	if (!AttackSound->IsPlayable()) return;

	UGameplayStatics::PlaySound2D(this, AttackSound);
}
