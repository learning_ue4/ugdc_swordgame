// Fill out your copyright notice in the Description page of Project Settings.


#include "Pickup.h"
#include "UGDC_SwordGame/Player/MainCharacter.h"

APickup::APickup()
{
}

void APickup::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
                             AActor* OtherActor,
                             UPrimitiveComponent* OtherComp,
                             int32 OtherBodyIndex,
                             bool bFromSweep,
                             const FHitResult& SweepResult)
{
    auto MainCharacter = Cast<AMainCharacter>(OtherActor);

    if (!IsValid(MainCharacter)) return;

    Super::OnOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);

    const auto PickerCharacter = Cast<AMainCharacter>(OtherActor);

    if (IsValid(PickerCharacter))
    {
        auto PickerCharacterController = PickerCharacter->GetPlayerController();

        if (IsValid(PickerCharacterController))
        {
            PickerCharacterController->SetUsableItemLocation(GetActorLocation());
            PickerCharacterController->DisplayUseItemWidget();
        }
    }
}

void APickup::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
                           AActor* OtherActor,
                           UPrimitiveComponent* OtherComp,
                           int32 OtherBodyIndex)
{
	Super::OnOverlapEnd(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);    
	
    const auto PickerCharacter = Cast<AMainCharacter>(OtherActor);

    if (IsValid(PickerCharacter))
    {
        auto PickerCharacterController = PickerCharacter->GetPlayerController();

        if (IsValid(PickerCharacterController))
        {
            PickerCharacterController->HideUseItemWidget();
        }
    }
}

void APickup::OnInteract(AMainCharacter* PickingUpCharacter)
{
	if (IsValid(PickingUpCharacter))
	{
        auto PickerCharacterController = PickingUpCharacter->GetPlayerController();

        if (IsValid(PickerCharacterController))
        {
            PickerCharacterController->HideUseItemWidget();
        }
	}
}

FString APickup::GetInteractingActionName_Implementation()
{
    return TEXT("Take");
}

FString APickup::GetInteractingActionMessage_Implementation()
{
    return PickupMessage;
}
