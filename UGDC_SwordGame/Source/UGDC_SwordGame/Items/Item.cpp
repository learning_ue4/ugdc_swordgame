// Fill out your copyright notice in the Description page of Project Settings.

#include "Item.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Sound/SoundCue.h"

// Sets default values
AItem::AItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InitializeCollisionAndMeshComponents();
	InitializeIdleParticleSystemComponent();

	OverlapSound = nullptr;
	bIsRotating = false;
}

void AItem::InitializeCollisionAndMeshComponents()
{
	CollisionVolume = CreateDefaultSubobject<USphereComponent>(TEXT("ItemCollisionVolume"));
	RootComponent = CollisionVolume;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ItemMeshComponent"));
	MeshComponent->SetupAttachment(RootComponent);
}

void AItem::InitializeIdleParticleSystemComponent()
{
	IdleParticleSystemComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("IdleParticleSystemComponent"));
	IdleParticleSystemComponent->SetupAttachment(RootComponent);

	OverlapParticleSystem = nullptr;	
}

// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();

	CollisionVolume->OnComponentBeginOverlap.AddDynamic(this, &AItem::OnOverlapBegin);
	CollisionVolume->OnComponentEndOverlap.AddDynamic(this, &AItem::OnOverlapEnd);	
}

// Called every frame
void AItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (bIsRotating)
	{
		SetRotation(DeltaTime);
	}
}

void AItem::SetRotation(const float& DeltaTime)
{
	auto Rotation = GetActorRotation();
	Rotation.Yaw += DeltaTime * RotationSpeed;
	SetActorRotation(Rotation);
}

void AItem::DeactivateIdleParticleSystem()
{
	if (IsValid(IdleParticleSystemComponent))
	{
		IdleParticleSystemComponent->Deactivate();
	}
}

void AItem::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (IsValid(OverlapParticleSystem))
	{
		const auto World = GetWorld();		

		if (World != nullptr)
		{
			const auto Location = GetActorLocation();
			UGameplayStatics::SpawnEmitterAtLocation(World, OverlapParticleSystem, Location, FRotator(.0F), true);			
		}
	}
	
	PlayOverlapSound();
}

void AItem::PlayOverlapSound() const
{
	if (!IsValid(OverlapSound)) return;	

	UGameplayStatics::PlaySound2D(this, OverlapSound);
}

void AItem::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex) { }

