// Fill out your copyright notice in the Description page of Project Settings.


#include "Explosive.h"
#include "UGDC_SwordGame/Player/MainCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"

AExplosive::AExplosive()
{
    InitializeParameters();
    CalculateDamage();
}

void AExplosive::InitializeParameters()
{
    DamageBase = 15.0F;
    DamageScatter = 5.0F;
}

void AExplosive::CalculateDamage()
{
    auto ScatteredAbsValue = FMath::FRandRange(.0F, DamageScatter);
    const auto bIsPlus = FMath::RandBool();

    if (!bIsPlus) ScatteredAbsValue *= -1;
	
    auto Result = DamageBase + ScatteredAbsValue;
    if (Result < 0) Result = 0;
       
    Damage = Result;	
}

void AExplosive::OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
                                AActor* OtherActor,
                                UPrimitiveComponent* OtherComp,
                                int32 OtherBodyIndex,
                                bool bFromSweep,
                                const FHitResult& SweepResult)
{
    if (!IsValid(OtherActor)) return;
	if (OtherComp == nullptr) return;
	
	const auto ExplodedCharacter = Cast<AGameCharacter>(OtherActor);

    if (!IsValid(ExplodedCharacter)) return;

    const auto CapsuleComponent = Cast<UCapsuleComponent>(OtherComp);

    if (CapsuleComponent == nullptr) return;
	
    Super::OnOverlapBegin(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult);
    UGameplayStatics::ApplyDamage(OtherActor, Damage, nullptr, this, DamageTypeClass);
    Destroy();
}

void AExplosive::OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
                              AActor* OtherActor,
                              UPrimitiveComponent* OtherComp,
                              int32 OtherBodyIndex)
{
    Super::OnOverlapEnd(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex);
}
