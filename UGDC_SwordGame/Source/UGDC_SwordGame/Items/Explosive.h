// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "Explosive.generated.h"

/**
 * An explosive item (bomb) damaging player on collision
 */
UCLASS()
class UGDC_SWORDGAME_API AExplosive : public AItem
{
	GENERATED_BODY()

public:
	AExplosive();	

private:
	/* Base value for damage calculation */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ExplosiveDamage, meta = (AllowPrivateAccess = "true"))
	float DamageBase;

	/* Scatter value for damage calculation */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ExplosiveDamage, meta = (AllowPrivateAccess = "true"))
	float DamageScatter;

	/* Actual damage dealt to the player */
	float Damage;

	/* Events occured on start colliding (overlapping) with item */
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                            AActor* OtherActor,
	                            UPrimitiveComponent* OtherComp,
	                            int32 OtherBodyIndex,
	                            bool bFromSweep,
	                            const FHitResult& SweepResult) override;

	/* Events occured on end colliding (overlapping) with item */
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	                          AActor* OtherActor,
	                          UPrimitiveComponent* OtherComp,
	                          int32 OtherBodyIndex) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UDamageType> DamageTypeClass;

	/* Initialize default values of class fields */
	void InitializeParameters();

	/* Sets random damage in range Base Damage +/- (0 ~ DamageScatter) */
	void CalculateDamage();	
};
