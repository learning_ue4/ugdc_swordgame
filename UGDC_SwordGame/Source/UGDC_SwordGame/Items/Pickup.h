// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "UGDC_SwordGame/Common/InteractInterface.h"
#include "Pickup.generated.h"

/**
 * An item to be placed in inventory
 */
UCLASS(Abstract)
class UGDC_SWORDGAME_API APickup : public AItem, public IInteractInterface
{
	GENERATED_BODY()
	
public:
	APickup();

	/* Actions performed when item is picked up */
	void OnInteract(class AMainCharacter* PickingUpCharacter) override;

	/* Name of the interaction action to be displayed on a widget */
	FString GetInteractingActionName_Implementation() override;

	/* Name (description) of the interacting object to be displayed on a widget */
	FString GetInteractingActionMessage_Implementation() override;
	
protected:
	/* Message when player overlaps with item he can pick up (item's name of brief description) */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = PickupMessage)
	FString PickupMessage;
	
	/* Events occured on start colliding (overlapping) with item */
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                            AActor* OtherActor,
	                            UPrimitiveComponent* OtherComp,
	                            int32 OtherBodyIndex,
	                            bool bFromSweep,
	                            const FHitResult& SweepResult) override;

	/* Events occured on end colliding (overlapping) with item */
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	                          AActor* OtherActor,
	                          UPrimitiveComponent* OtherComp,
	                          int32 OtherBodyIndex) override;
};
