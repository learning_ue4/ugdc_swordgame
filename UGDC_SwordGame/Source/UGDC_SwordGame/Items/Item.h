// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Item.generated.h"

/*
 * An item in general - smth that could be picked up or
 * interact with player another way, but doesn't have
 * an AI and behavior logic other than on overlapping (colliding)
 */
UCLASS(Abstract)
class UGDC_SWORDGAME_API AItem : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AItem();

	FORCEINLINE FString GetItemName() const { return Name; };

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* Events occured on start colliding (overlapping) with item */
	UFUNCTION()
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                            AActor* OtherActor,
	                            UPrimitiveComponent* OtherComp,
	                            int32 OtherBodyIndex,
	                            bool bFromSweep,
	                            const FHitResult& SweepResult);

	/* Events occured on end colliding (overlapping) with item */
	UFUNCTION()
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	                          AActor* OtherActor,
	                          UPrimitiveComponent* OtherComp,
	                          int32 OtherBodyIndex);

	/* Initialize item's particle system component */
	virtual void InitializeIdleParticleSystemComponent();

	/* Deactivate item's idle particle FX */
	void DeactivateIdleParticleSystem();

	FORCEINLINE void StopRotation() { bIsRotating = false; }

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	UPROPERTY(EditDefaultsOnly, Category = SavedData)
	FString Name;

	/* Particle system component used when the item is in idle state */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item | Particles", meta = (AllowPrivateAccess = "true"))
	class UParticleSystemComponent* IdleParticleSystemComponent;

	/* Base shape collision */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item | Collision", meta = (AllowPrivateAccess = "true"))
	class USphereComponent* CollisionVolume;

	/* Base mesh component */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item | MeshComponent", meta = (AllowPrivateAccess = "true"))
	class UStaticMeshComponent* MeshComponent;

	/* Particle system used for consumption effect */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item | Particles", meta = (AllowPrivateAccess = "true"))
	class UParticleSystem* OverlapParticleSystem;

	/* SFX played on consumption */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item | Sounds", meta = (AllowPrivateAccess = "true"))
	class USoundCue* OverlapSound;

	/* Is item rotating when idle */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Rotation", meta = (AllowPrivateAccess = "true"))
	bool bIsRotating;

	/* Idle rotation speed */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item | Rotation", meta = (AllowPrivateAccess = "true"))
	float RotationSpeed;

	/* Initialize item's collision and mesh components */
	void InitializeCollisionAndMeshComponents();

	/* Set item's rotation */
	void SetRotation(const float& DeltaSeconds);

	/* Play sound when item is consumed */
	void PlayOverlapSound() const;
};
