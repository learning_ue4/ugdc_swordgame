// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "Consumable.generated.h"

/*
 * An item to be consumed in place
 * with consuming effect defined in blueprint
 */
UCLASS(Abstract)
class UGDC_SWORDGAME_API AConsumable : public AItem
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AConsumable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	/* Blueprint event firing when item is overlapped */
	UFUNCTION(BlueprintImplementableEvent, Category = Consumable)
	void OnPickupBP(class AMainCharacter* Target);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

private:
	/* Can the item be consumed - updates in OnPickupBp */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Consumable, meta = (AllowPrivateAccess = "true"))
	bool bCanBeConsumed;

	/* Events occured on start colliding (overlapping) with item */
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                            AActor* OtherActor,
	                            UPrimitiveComponent* OtherComp,
	                            int32 OtherBodyIndex,
	                            bool bFromSweep,
	                            const FHitResult& SweepResult) override;

	/* Events occured on end colliding (overlapping) with item */
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	                          AActor* OtherActor,
	                          UPrimitiveComponent* OtherComp,
	                          int32 OtherBodyIndex) override;
};
