// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "Pickup.h"
#include "Weapon.generated.h"

class AEnemy;
/*
 * A type of weapon
 */
UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	EWT_Melee UMETA(DisplayName = "Melee"),
	EWT_Ranged UMETA(DisplayName = "Ranged"),
	EWT_MAX UMETA(DisplayName = "DefaultMax")
};

/*
 * A state that weapon can be in, it depends
 * on if's equipped and deals damage or lying
 * as a pickup or other
 */
UENUM(BlueprintType)
enum class EWeaponState : uint8
{
	EWS_Pickup UMETA(DisplayName = "Pickup"),
	EWS_Equipped UMETA(DisplayName = "Equipped"),
	EWS_MAX UMETA(DisplayName = "DefaultMAX")
};

/*
 * Weapon, that could be picked up and equipped by player
 */
UCLASS()
class UGDC_SWORDGAME_API AWeapon final : public APickup
{
	GENERATED_BODY()
	
public:
	AWeapon();

protected:
	virtual void BeginPlay() override;

public:
	FORCEINLINE void SetWeaponState(const EWeaponState& StateToSet) { State = StateToSet; }
	FORCEINLINE EWeaponState GetWeaponState() const { return State; }
	FORCEINLINE EWeaponType GetWeaponType() const { return Type; }

	void OnInteract(AMainCharacter* Interacting) override;

	/* Plays sound when attacking with this weapon */
	void PlayAttackSound() const;

private:
	/* A state that weapon is currently in */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item | State", meta = (AllowPrivateAccess = "true"))
	EWeaponState State;

	/* Type of weapon (i.e. melee, ranged, etc.) */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item | Type", meta = (AllowPrivateAccess = "true"))
	EWeaponType Type;

	/* A component of weapon's mesh - a weapon should use skeletal mesh
	 * instead of static mesh of a standard pickup
	 */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item | SkeletalMesh", meta = (AllowPrivateAccess = "true"))
	class USkeletalMeshComponent* SkeletalMeshComponent;

	/* SFX played when the weapon is equipped */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item | SFX", meta = (AllowPrivateAccess = "true"))
	class USoundCue* OnEquipSound;

	/* Particle system component used when weapon in equipped */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item | Particles", meta = (AllowPrivateAccess = "true"))
	class UParticleSystemComponent* EquippedParticleSystemComponent;

	/* Combat collision box used when enemy is being hit by it */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Weapon | Combat", meta = (AllowPrivateAccess = "true"))
	class UBoxComponent* CombatCollisionBox;

	/* Base damage dealt by weapon for damage calculations */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Combat", meta = (AllowPrivateAccess = "true"))
	float DamageBase;

	/* Scatter damage dealt by weapon for damage calculations */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Combat", meta = (AllowPrivateAccess = "true"))
	float DamageScatter;

	/* Weapon's damage type */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon | Combat", meta = (AllowPrivateAccess = "true"))
	TSubclassOf<UDamageType> DamageTypeClass;

	/* Controller will be used to deal damage */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon | Combat", meta = (AllowPrivateAccess = "true"))
	AController* WeaponInstigator;

	/* Sound played when attacking with this weapon */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon | Combat", meta = (AllowPrivateAccess = "true"))
	class USoundCue* AttackSound;
	
	/* Actual damage dealt by weapon */
	float Damage;
	
	/* Init skeletal mesh component and attach it to root component */
	void InitializeSkeletalMesh();

	/* Initialize defaults */
	void InitDefaults();

	/* Initialize weapon's collision box */
	void InitializeCombatCollisionBox();

	/* Initialize item's particle system component */
	virtual void InitializeIdleParticleSystemComponent() override;

	/* Set collision weapon's box default parameters */
	void SetupCollisionBox();

	/* Events occured on start colliding (overlapping) with item */
	virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                            AActor* OtherActor,
	                            UPrimitiveComponent* OtherComp,
	                            int32 OtherBodyIndex,
	                            bool bFromSweep,
	                            const FHitResult& SweepResult) override;

	/* Events occured on end colliding (overlapping) with item */
	virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	                          AActor* OtherActor,
	                          UPrimitiveComponent* OtherComp,
	                          int32 OtherBodyIndex) override;

	/* Events occured on start colliding (overlapping) with combat collision box (hit with a weapon) */
	UFUNCTION()
	void OnCombatCollisionBoxOverlapBegin(UPrimitiveComponent* OverlappedComponent,
	                                      AActor* OtherActor,
	                                      UPrimitiveComponent* OtherComp,
	                                      int32 OtherBodyIndex,
	                                      bool bFromSweep,
	                                      const FHitResult& SweepResult);

	/* Events occured on end colliding (overlapping) with with combat collision box (hit with a weapon) */
	UFUNCTION()
	void OnCombatCollisionBoxOverlapEnd(UPrimitiveComponent* OverlappedComponent,
	                                    AActor* OtherActor,
	                                    UPrimitiveComponent* OtherComp,
	                                    int32 OtherBodyIndex);

	/* Equip the weapon */
	void Equip(class AMainCharacter* Character);
		
	/* Actual damage dealt by enemy */
	float CalculateDamage() const;
	
	/* Play SFX when equipped the weapon */
	void PlayOnEquipSound() const;

	/* Apply calculated damage to target */
	void ApplyDamageToTarget(AActor* Target);
	
	/* Play SFX when enemy is being hit */
	void PlayEnemyHitSound(const AEnemy* Enemy) const;

	/* Play particle effect when enemy is being hit */
	void PlayEnemyHitEffect(const AEnemy* Enemy) const;

	/* Activate collision - called from blueprint */
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	void ActivateCollision();

	/* Deactivate collision - called from blueprint */
	UFUNCTION(BlueprintCallable, meta = (AllowPrivateAccess = "true"))
	void DeactivateCollision();
};
