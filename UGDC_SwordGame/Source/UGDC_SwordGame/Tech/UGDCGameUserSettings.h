// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameUserSettings.h"
#include "UGDCGameUserSettings.generated.h"

/**
 * 
 */
UCLASS()
class UGDC_SWORDGAME_API UUGDCGameUserSettings : public UGameUserSettings
{
	GENERATED_BODY()

public:
	virtual void ApplySettings(bool bCheckForCommandLineOverrides) override;

	FORCEINLINE bool IsGamePadUsed() const { return bIsGamePadUsed; }

protected:
	UPROPERTY(Config)
	bool bIsGamePadUsed;
};
