// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "UGDC_SwordGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, UGDC_SwordGame, "UGDC_SwordGame" );
