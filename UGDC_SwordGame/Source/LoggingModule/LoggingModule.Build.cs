using UnrealBuildTool;

public class LoggingModule : ModuleRules
{
    public LoggingModule(ReadOnlyTargetRules Target) : base (Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "UGDC_SwordGame" });

        PublicIncludePaths.AddRange(new string[] { "LoggingModule/Public" });
        PrivateIncludePaths.AddRange(new string[] { "LoggingModule/Private" });
    }
}