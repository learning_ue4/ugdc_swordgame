// Fill out your copyright notice in the Description page of Project Settings.


#include "Logger.h"

void ULogger::LogWarning(const char* Message)
{
	UE_LOG(LogTemp, Warning, TEXT("%hs"), Message);
}

void ULogger::LogWarning(FString Message)
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *Message);
}