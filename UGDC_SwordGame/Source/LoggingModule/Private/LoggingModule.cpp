#include "LoggingModule.h"

DEFINE_LOG_CATEGORY(LoggingModule);

#define LOCTEXT_NAMESPACE "FLoggingModule"

void FLoggingModule::StartupModule()
{
    UE_LOG(LoggingModule, Display, TEXT("Logging module has started"));
}

void FLoggingModule::ShutdownModule()
{
    UE_LOG(LoggingModule, Display, TEXT("Logginig module has shut down"));
}

#undef LOCTEXT_NAMESPACE
IMPLEMENT_MODULE(FLoggingModule, LoggingModule)