// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Logger.generated.h"

/**
 * Just another logger
 */
UCLASS()
class LOGGINGMODULE_API ULogger : public UObject
{
	GENERATED_BODY()
	
public:
	static void LogWarning(const char* Message);

	static void LogWarning(FString Message);
	
};
