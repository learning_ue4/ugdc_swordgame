A simple third-person action game made as a part of Udemy course "Unreal Engine The Ultimate Game Developer" https://www.udemy.com/course/unreal-engine-the-ultimate-game-developer-course/

Production of the game is currently in progress, I add new features as I go through the course.
Features are from the course content as well as new, based on the course or invented by myself.

Assets used in this game are free and taken from various sources:

* UE4 Marketplace: https://www.unrealengine.com/marketplace/en-US/store  
* Official course github: https://github.com/DruidMech/LearnCPPForUnrealEngineUnit2 
* Mixamo (characters and animations): https://www.mixamo.com/#/ 

Currently using Unreal Engine v.4.24.

New info is added along with the progress of making this game.

